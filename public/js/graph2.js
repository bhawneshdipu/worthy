Chart.defaults.global.elements.point.radius = 5;
Chart.defaults.global.elements.point.hitRadius = 50;
Chart.defaults.global.elements.point.hoverBorderWidth = 50;
var myAlexaChartTextAndRank2;
var myAlexaChartDelta2;
var myAlexaChartCountryRank2;

var myAlexaChartDailyUniqueVisits2;
var myAlexaChartDailyPageViews2;
var myAlexaChartIncomePerDay2;
var myAlexaChartEstimatedValue2;


var mySumrushChartRk2;
var mySumrushChartOr2;
var mySumrushChartOt2;
var mySumrushChartOc2;

var myResponseTimeChart2;
var myFacebookCommentChart2;
var myFacebookShareChart2;
var myDesktopSpeedScoreChart2;
var myMobileSpeedScoreChart2;

var myMobileUsabilityScoreChart2;
var myPinterestChart2;
var myStumbleuponChart2;




function createResponseChart2(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myResponseTimeChart2').html('');
    createResponseTimeChart2(labels,data);

}
function createFacebookChart2(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myFacebookCommentChart2').html('');
    $('#myFacebookShareChart2').html('');
    createFacebookShareChart2(labels,data);
    createFacebookCommentChart2(labels,data);

}

function createSpeedScoreChart2(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myDesktopSpeedScoreChart2').html('');
    $('#myMobileSpeedScoreChart2').html('');
    createDesktopSpeedScoreChart2(labels,data);
    createMobileSpeedScoreChart2(labels,data);

}
function createUsabilityScoreChart2(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myMobileUsabilityScoreChart2').html('');
    createMobileUsabilityScoreChart2(labels,data);

}
function createPinterestCountChart2(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myPinterestChart2').html('');
    createPinterestChart2(labels,data);

}

function createStumbleuponCountChart2(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myStumbleuponChart2').html('');
    createStumbleuponChart2(labels,data);

}




function createAlexaChart2(labels,data) {
 data=JSON.parse(data);
 labels=JSON.parse(labels);
    $('#myAlexaChartTextAndRank2').html('');
    $('#myAlexaChartDelta2').html('');
    $('#myAlexaChartCountryRank2').html('');
    createAlexaChartTextAndRank2(labels,data);
    createAlexaChartDelta2(labels,data);
    createAlexaChartCountryRank2(labels,data);

    $('#myAlexaChartDailyUniqueVisits2').html('');
    $('#myAlexaChartDailyPageViews2').html('');
    $('#myAlexaChartIncomePerDay2').html('');
    $('#myAlexaChartEstimatedValue2').html('');

    createAlexaChartDailyUniqueVisits2(labels,data);
    createAlexaChartDailyPageViews2(labels,data);
    createAlexaChartIncomePerDay2(labels,data);
    createAlexaChartEstimatedValue2(labels,data);




}
function createSumrushChart2(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#mySumrushChartRk2').html('');
    $('#mySumrushChartOr2').html('');
    $('#mySumrushChartOt2').html('');
    $('#mySumrushChartOc2').html('');

    createmySumrushChartRk2(labels,data);
    createmySumrushChartOr2(labels,data);
    createmySumrushChartOt2(labels,data);
    createmySumrushChartOc2(labels,data);



}



function getData2(days) {
    var labesl=null;
    var data=null;
    console.log($('#mywebsite2').val());
    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getAlexaXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getAlexaYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createAlexaChart2(xdata,ydata);
                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------Sumrush-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getSumrushXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getSumrushYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createSumrushChart2(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------Facebook-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYFacebookCount",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createFacebookChart2(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

/*---speed score---*/



    /*-----------Facebook-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYSpeedScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createSpeedScoreChart2(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });




    /*-----------Mobile Usability-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYMobileUsablityScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createUsabilityScoreChart2(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });



    /*-----------Pinterest Usability-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYPinterest",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createPinterestCountChart2(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });





    /*-----------Stumbleupon -------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYStumbleupon",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createStumbleuponCountChart2(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });




    /*-----------Response TIme -------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYResponseTime",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createResponseChart2(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

}

function updateData2(days) {
    var labesl=null;
    var data=null;
    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getAlexaXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getAlexaYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myAlexaChartTextAndRank2.data.labels=xdata;
                    myAlexaChartTextAndRank2.data.datasets[0].data=ydata.rank;
                    myAlexaChartTextAndRank2.data.datasets[1].data=ydata.text;
                    myAlexaChartDelta2.data.datasets[0].data=ydata.delta;
                    myAlexaChartCountryRank2.data.datasets[0].data=ydata.country_rank;

                    myAlexaChartTextAndRank2.update();

                    myAlexaChartDelta2.update();
                    myAlexaChartCountryRank2.update();

                    myAlexaChartDailyUniqueVisits2.data.datasets[0].data=ydata.daily_unique_visits;
                    myAlexaChartDailyPageViews2.data.datasets[0].data=ydata.daily_page_views;
                    myAlexaChartIncomePerDay2.data.datasets[0].data=ydata.income_per_day;
                    myAlexaChartEstimatedValue2.data.datasets[0].data=ydata.estimated_value;


                    myAlexaChartDailyUniqueVisits2.data.labels=xdata;
                    myAlexaChartDailyPageViews2.data.labels=xdata;
                    myAlexaChartIncomePerDay2.data.labels=xdata;
                    myAlexaChartEstimatedValue2.data.labels=xdata;

                    myAlexaChartDailyUniqueVisits2.update();
                    myAlexaChartDailyPageViews2.update();
                    myAlexaChartIncomePerDay2.update();
                    myAlexaChartEstimatedValue2.update();







                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

    /*-----------Sumrush-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getSumrushXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getSumrushYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    mySumrushChartRk2.data.labels=xdata;
                    mySumrushChartRk2.data.datasets[0].data=ydata.Rk;
                    mySumrushChartRk2.update();

                    mySumrushChartOr2.data.labels=xdata;
                    mySumrushChartOr2.data.datasets[0].data=ydata.Or;
                    mySumrushChartOr2.update();

                    mySumrushChartOt2.data.labels=xdata;
                    mySumrushChartOt2.data.datasets[0].data=ydata.Ot;
                    mySumrushChartOt2.update();

                    mySumrushChartOc2.data.labels=xdata;
                    mySumrushChartOc2.data.datasets[0].data=ydata.Oc;
                    mySumrushChartOc2.update();

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });




    /*-----------Facebook-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYFacebookCount",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myFacebookCommentChart2.data.labels=xdata;
                    myFacebookCommentChart2.data.datasets[0].data=ydata.comment;
                    myFacebookCommentChart2.update();

                    myFacebookShareChart2.data.labels=xdata;
                    myFacebookShareChart2.data.datasets[0].data=ydata.share;
                    myFacebookShareChart2.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });



    /*-----------Speed Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYSpeedScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myDesktopSpeedScoreChart2.data.labels=xdata;
                    myDesktopSpeedScoreChart2.data.datasets[0].data=ydata.desktop;
                    myDesktopSpeedScoreChart2.update();

                    myMobileSpeedScoreChart2.data.labels=xdata;
                    myMobileSpeedScoreChart2.data.datasets[0].data=ydata.mobile;
                    myMobileSpeedScoreChart2.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });






    /*-----------Usability Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYMobileUsablityScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myMobileUsabilityScoreChart2.data.labels=xdata;
                    myMobileUsabilityScoreChart2.data.datasets[0].data=ydata;
                    myMobileUsabilityScoreChart2.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });





    /*-----------Pinterest  Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYPinterest",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myPinterestChart2.data.labels=xdata;
                    myPinterestChart2.data.datasets[0].data=ydata;
                    myPinterestChart2.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------getYStumbleupon  Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYStumbleupon",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myStumbleuponChart2.data.labels=xdata;
                    myStumbleuponChart2.data.datasets[0].data=ydata;
                    myStumbleuponChart2.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------getYResponseTime  Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/2/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite2').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getYResponseTime",
                data: {
                    'days':days,
                    'website':$('#mywebsite2').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myResponseTimeChart2.data.labels=xdata;
                    myResponseTimeChart2.data.datasets[0].data=ydata;
                    myResponseTimeChart2.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

}



$(document).ready(function(){
    getData2(30);
});
$('#days').on('change',function(){
    updateData2($(this).val());
});













/*********************chat js***************/

function createAlexaChartTextAndRank2(labels,data) {

    console.log("myAlexaChartTextAndRank2");
    console.log(labels);
    console.log(data);

    var ctx = document.getElementById("myAlexaChartTextAndRank2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartTextAndRank2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.rank,
                    label: "RANK",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                },
                {
                    data: data.text,
                    label: "TEXT",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black",
                        reverse:true
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}





function createAlexaChartDelta2(labels,data) {


    var ctx = document.getElementById("myAlexaChartDelta2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartDelta2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.delta,
                    label: "DELTA",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartCountryRank2(labels,data) {


    var ctx = document.getElementById("myAlexaChartCountryRank2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartCountryRank2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.country_rank,
                    label: "DELTA:"+data.country_name[0],
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}
/*=====================================================new Alexa {age Stats===================*/


function createAlexaChartDailyUniqueVisits2(labels,data) {


    var ctx = document.getElementById("myAlexaChartDailyUniqueVisits2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartDailyUniqueVisits2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.daily_unique_visits,
                    label: "DailyUniqueVisits",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartDailyPageViews2(labels,data) {


    var ctx = document.getElementById("myAlexaChartDailyPageViews2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartDailyPageViews2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.daily_page_views,
                    label: "DailyPageViews",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartIncomePerDay2(labels,data) {


    var ctx = document.getElementById("myAlexaChartIncomePerDay2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartIncomePerDay2= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.income_per_day,
                    label: "IncomePerDay",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartEstimatedValue2(labels,data) {


    var ctx = document.getElementById("myAlexaChartEstimatedValue2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartEstimatedValue2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.estimated_value,
                    label: "EstimatedValue",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



/*================================================================================================*/


function createmySumrushChartRk2(labels,data) {


    var ctx = document.getElementById("mySumrushChartRk2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartRk2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Rk,
                    label: "RANK",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createmySumrushChartOr2(labels,data) {


    var ctx = document.getElementById("mySumrushChartOr2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartOr2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Or,
                    label: "Or",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createmySumrushChartOt2(labels,data) {


    var ctx = document.getElementById("mySumrushChartOt2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartOt2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Ot,
                    label: "Ot",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createmySumrushChartOc2(labels,data) {


    var ctx = document.getElementById("mySumrushChartOc2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartOc2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Oc,
                    label: "Oc",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}

/*-------Facebook--------*/


function createFacebookShareChart2(labels,data) {


    var ctx = document.getElementById("myFacebookShareChart2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myFacebookShareChart2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.share,
                    label: "Facebook Share",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



function createFacebookCommentChart2(labels,data) {


    var ctx = document.getElementById("myFacebookCommentChart2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myFacebookCommentChart2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.comment,
                    label: "Facebook Comment",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



/*---------google Speed socre--*/



function createFacebookShareChart2(labels,data) {


    var ctx = document.getElementById("myFacebookShareChart2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myFacebookShareChart2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.share,
                    label: "Facebook Share",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


/*---speed score --*/

/*---------google Speed socre--*/



function createDesktopSpeedScoreChart2(labels,data) {


    var ctx = document.getElementById("myDesktopSpeedScoreChart2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myDesktopSpeedScoreChart2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.desktop,
                    label: "Desktop Speed",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



function createMobileSpeedScoreChart2(labels,data) {


    var ctx = document.getElementById("myMobileSpeedScoreChart2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myMobileSpeedScoreChart2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.mobile,
                    label: "Mobile Speed",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}






function createMobileUsabilityScoreChart2(labels,data) {


    var ctx = document.getElementById("myMobileUsabilityScoreChart2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myMobileUsabilityScoreChart2 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Mobile Usability Score",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}





function createPinterestChart2(labels,data) {


    var ctx = document.getElementById("myPinterestChart2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myPinterestChart2= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Pinterest ",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}




function createStumbleuponChart2(labels,data) {


    var ctx = document.getElementById("myStumbleuponChart2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myStumbleuponChart2= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Stumbleupon",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



function createResponseTimeChart2(labels,data) {


    var ctx = document.getElementById("myResponseTimeChart2").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myResponseTimeChart2= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Response Time",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


