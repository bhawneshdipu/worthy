Chart.defaults.global.elements.point.radius = 5;
Chart.defaults.global.elements.point.hitRadius = 50;
Chart.defaults.global.elements.point.hoverBorderWidth = 50;
var myAlexaChartTextAndRank;
var myAlexaChartDelta;
var myAlexaChartCountryRank;

var myAlexaChartDailyUniqueVisits;
var myAlexaChartDailyPageViews;
var myAlexaChartIncomePerDay;
var myAlexaChartEstimatedValue;


var mySumrushChartRk;
var mySumrushChartOr;
var mySumrushChartOt;
var mySumrushChartOc;

var myResponseTimeChart;
var myFacebookCommentChart;
var myFacebookShareChart;
var myDesktopSpeedScoreChart;
var myMobileSpeedScoreChart;

var myMobileUsabilityScoreChart;
var myPinterestChart;
var myStumbleuponChart;




function createResponseChart(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myResponseTimeChart').html('');
    createResponseTimeChart(labels,data);

}
function createFacebookChart(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myFacebookCommentChart').html('');
    $('#myFacebookShareChart').html('');
    createFacebookShareChart(labels,data);
    createFacebookCommentChart(labels,data);

}

function createSpeedScoreChart(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myDesktopSpeedScoreChart').html('');
    $('#myMobileSpeedScoreChart').html('');
    createDesktopSpeedScoreChart(labels,data);
    createMobileSpeedScoreChart(labels,data);

}
function createUsabilityScoreChart(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myMobileUsabilityScoreChart').html('');
    createMobileUsabilityScoreChart(labels,data);

}
function createPinterestCountChart(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myPinterestChart').html('');
    createPinterestChart(labels,data);

}

function createStumbleuponCountChart(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myStumbleuponChart').html('');
    createStumbleuponChart(labels,data);

}




function createAlexaChart(labels,data) {
 data=JSON.parse(data);
 labels=JSON.parse(labels);
    $('#myAlexaChartTextAndRank').html('');
    $('#myAlexaChartDelta').html('');
    $('#myAlexaChartCountryRank').html('');
    createAlexaChartTextAndRank(labels,data);
    createAlexaChartDelta(labels,data);
    createAlexaChartCountryRank(labels,data);

    $('#myAlexaChartDailyUniqueVisits').html('');
    $('#myAlexaChartDailyPageViews').html('');
    $('#myAlexaChartIncomePerDay').html('');
    $('#myAlexaChartEstimatedValue').html('');

    createAlexaChartDailyUniqueVisits(labels,data);
    createAlexaChartDailyPageViews(labels,data);
    createAlexaChartIncomePerDay(labels,data);
    createAlexaChartEstimatedValue(labels,data);




}
function createSumrushChart(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#mySumrushChartRk').html('');
    $('#mySumrushChartOr').html('');
    $('#mySumrushChartOt').html('');
    $('#mySumrushChartOc').html('');

    createmySumrushChartRk(labels,data);
    createmySumrushChartOr(labels,data);
    createmySumrushChartOt(labels,data);
    createmySumrushChartOc(labels,data);



}



function getData(days) {
    var labesl=null;
    var data=null;
    console.log($('#mywebsite').val());
    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getAlexaXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getAlexaYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createAlexaChart(xdata,ydata);
                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------Sumrush-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getSumrushXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getSumrushYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createSumrushChart(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------Facebook-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYFacebookCount",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createFacebookChart(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

/*---speed score---*/



    /*-----------Facebook-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYSpeedScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createSpeedScoreChart(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });




    /*-----------Mobile Usability-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYMobileUsablityScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createUsabilityScoreChart(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });



    /*-----------Pinterest Usability-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYPinterest",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createPinterestCountChart(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });





    /*-----------Stumbleupon -------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYStumbleupon",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createStumbleuponCountChart(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });




    /*-----------Response TIme -------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYResponseTime",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createResponseChart(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

}

function updateData(days) {
    var labesl=null;
    var data=null;
    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getAlexaXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getAlexaYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myAlexaChartTextAndRank.data.labels=xdata;
                    myAlexaChartTextAndRank.data.datasets[0].data=ydata.rank;
                    myAlexaChartTextAndRank.data.datasets[1].data=ydata.text;
                    myAlexaChartDelta.data.datasets[0].data=ydata.delta;
                    myAlexaChartCountryRank.data.datasets[0].data=ydata.country_rank;

                    myAlexaChartTextAndRank.update();

                    myAlexaChartDelta.update();
                    myAlexaChartCountryRank.update();

                    myAlexaChartDailyUniqueVisits.data.datasets[0].data=ydata.daily_unique_visits;
                    myAlexaChartDailyPageViews.data.datasets[0].data=ydata.daily_page_views;
                    myAlexaChartIncomePerDay.data.datasets[0].data=ydata.income_per_day;
                    myAlexaChartEstimatedValue.data.datasets[0].data=ydata.estimated_value;


                    myAlexaChartDailyUniqueVisits.data.labels=xdata;
                    myAlexaChartDailyPageViews.data.labels=xdata;
                    myAlexaChartIncomePerDay.data.labels=xdata;
                    myAlexaChartEstimatedValue.data.labels=xdata;

                    myAlexaChartDailyUniqueVisits.update();
                    myAlexaChartDailyPageViews.update();
                    myAlexaChartIncomePerDay.update();
                    myAlexaChartEstimatedValue.update();







                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

    /*-----------Sumrush-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getSumrushXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getSumrushYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    mySumrushChartRk.data.labels=xdata;
                    mySumrushChartRk.data.datasets[0].data=ydata.Rk;
                    mySumrushChartRk.update();

                    mySumrushChartOr.data.labels=xdata;
                    mySumrushChartOr.data.datasets[0].data=ydata.Or;
                    mySumrushChartOr.update();

                    mySumrushChartOt.data.labels=xdata;
                    mySumrushChartOt.data.datasets[0].data=ydata.Ot;
                    mySumrushChartOt.update();

                    mySumrushChartOc.data.labels=xdata;
                    mySumrushChartOc.data.datasets[0].data=ydata.Oc;
                    mySumrushChartOc.update();

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });




    /*-----------Facebook-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYFacebookCount",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myFacebookCommentChart.data.labels=xdata;
                    myFacebookCommentChart.data.datasets[0].data=ydata.comment;
                    myFacebookCommentChart.update();

                    myFacebookShareChart.data.labels=xdata;
                    myFacebookShareChart.data.datasets[0].data=ydata.share;
                    myFacebookShareChart.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });



    /*-----------Speed Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYSpeedScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myDesktopSpeedScoreChart.data.labels=xdata;
                    myDesktopSpeedScoreChart.data.datasets[0].data=ydata.desktop;
                    myDesktopSpeedScoreChart.update();

                    myMobileSpeedScoreChart.data.labels=xdata;
                    myMobileSpeedScoreChart.data.datasets[0].data=ydata.mobile;
                    myMobileSpeedScoreChart.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });






    /*-----------Usability Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYMobileUsablityScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myMobileUsabilityScoreChart.data.labels=xdata;
                    myMobileUsabilityScoreChart.data.datasets[0].data=ydata;
                    myMobileUsabilityScoreChart.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });





    /*-----------Pinterest  Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYPinterest",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myPinterestChart.data.labels=xdata;
                    myPinterestChart.data.datasets[0].data=ydata;
                    myPinterestChart.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------getYStumbleupon  Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYStumbleupon",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myStumbleuponChart.data.labels=xdata;
                    myStumbleuponChart.data.datasets[0].data=ydata;
                    myStumbleuponChart.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------getYResponseTime  Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/getYResponseTime",
                data: {
                    'days':days,
                    'website':$('#mywebsite').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myResponseTimeChart.data.labels=xdata;
                    myResponseTimeChart.data.datasets[0].data=ydata;
                    myResponseTimeChart.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

}



$(document).ready(function(){
    getData(30);
});
$('#days').on('change',function(){
    updateData($(this).val());
});













/*********************chat js***************/

function createAlexaChartTextAndRank(labels,data) {


    var ctx = document.getElementById("myAlexaChartTextAndRank").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartTextAndRank = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.rank,
                    label: "RANK",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                },
                {
                    data: data.text,
                    label: "TEXT",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black",
                        reverse:true
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}





function createAlexaChartDelta(labels,data) {


    var ctx = document.getElementById("myAlexaChartDelta").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartDelta = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.delta,
                    label: "DELTA",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartCountryRank(labels,data) {


    var ctx = document.getElementById("myAlexaChartCountryRank").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartCountryRank = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.country_rank,
                    label: "DELTA:"+data.country_name[0],
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}
/*=====================================================new Alexa {age Stats===================*/


function createAlexaChartDailyUniqueVisits(labels,data) {


    var ctx = document.getElementById("myAlexaChartDailyUniqueVisits").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartDailyUniqueVisits = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.daily_unique_visits,
                    label: "DailyUniqueVisits",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartDailyPageViews(labels,data) {


    var ctx = document.getElementById("myAlexaChartDailyPageViews").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartDailyPageViews = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.daily_page_views,
                    label: "DailyPageViews",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartIncomePerDay(labels,data) {


    var ctx = document.getElementById("myAlexaChartIncomePerDay").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartIncomePerDay= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.income_per_day,
                    label: "IncomePerDay",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartEstimatedValue(labels,data) {


    var ctx = document.getElementById("myAlexaChartEstimatedValue").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartEstimatedValue = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.estimated_value,
                    label: "EstimatedValue",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



/*================================================================================================*/


function createmySumrushChartRk(labels,data) {


    var ctx = document.getElementById("mySumrushChartRk").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartRk = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Rk,
                    label: "RANK",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createmySumrushChartOr(labels,data) {


    var ctx = document.getElementById("mySumrushChartOr").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartOr = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Or,
                    label: "Or",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createmySumrushChartOt(labels,data) {


    var ctx = document.getElementById("mySumrushChartOt").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartOt = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Ot,
                    label: "Ot",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createmySumrushChartOc(labels,data) {


    var ctx = document.getElementById("mySumrushChartOc").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartOc = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Oc,
                    label: "Oc",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}

/*-------Facebook--------*/


function createFacebookShareChart(labels,data) {


    var ctx = document.getElementById("myFacebookShareChart").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myFacebookShareChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.share,
                    label: "Facebook Share",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



function createFacebookCommentChart(labels,data) {


    var ctx = document.getElementById("myFacebookCommentChart").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myFacebookCommentChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.comment,
                    label: "Facebook Comment",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



/*---------google Speed socre--*/



function createFacebookShareChart(labels,data) {


    var ctx = document.getElementById("myFacebookShareChart").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myFacebookShareChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.share,
                    label: "Facebook Share",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


/*---speed score --*/

/*---------google Speed socre--*/



function createDesktopSpeedScoreChart(labels,data) {


    var ctx = document.getElementById("myDesktopSpeedScoreChart").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myDesktopSpeedScoreChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.desktop,
                    label: "Desktop Speed",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



function createMobileSpeedScoreChart(labels,data) {


    var ctx = document.getElementById("myMobileSpeedScoreChart").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myMobileSpeedScoreChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.mobile,
                    label: "Mobile Speed",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}






function createMobileUsabilityScoreChart(labels,data) {


    var ctx = document.getElementById("myMobileUsabilityScoreChart").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myMobileUsabilityScoreChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Mobile Usability Score",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}





function createPinterestChart(labels,data) {


    var ctx = document.getElementById("myPinterestChart").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myPinterestChart= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Pinterest ",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}




function createStumbleuponChart(labels,data) {


    var ctx = document.getElementById("myStumbleuponChart").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myStumbleuponChart= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Stumbleupon",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



function createResponseTimeChart(labels,data) {


    var ctx = document.getElementById("myResponseTimeChart").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myResponseTimeChart= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Response Time",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


