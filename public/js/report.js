// process the form

$('form[name="report"]').submit(function(event) {
    event.preventDefault();
    var target = $(this).closest('form').find('#website').val();
    console.log(target);
    startReport(target);
});


function updateMyReport(){
    var target = $('#update_website').val();
    console.log(target);
    updateReport(target);
}
/*$('#updatebutton').on('click',function(){
    var target = $(this).closest('form').find('#update_website').val();
    console.log(target);
    updateReport(target);
});
*/
$('form[name="reportbar"]').submit(function(event) {
    event.preventDefault();
    var target = $(this).closest('form').find('#website').val();
    console.log(target);
    startReport(target);
});

function startReport(target){
    // show progress bar
    $('.progress').removeClass('hide');
    /* update the progress bar width */
    $('.progress-bar').css('width', '10%').attr('aria-valuenow', 10);
    /* and display the numeric value */
    $(".progress-bar").html('validate url 10%');

    // get the form data
    // there are many ways to get this data using jQuery (you can use the class or id also)
    var formData = {
        'website': target
    };

    // process the form
    getValidation(formData);

    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();

}
function updateReport(target){
    // show progress bar
    $('.progress').removeClass('hide');
    /* update the progress bar width */
    $('.progress-bar').css('width', '10%').attr('aria-valuenow', 10);
    /* and display the numeric value */
    $(".progress-bar").html('validate url 10%');

    // get the form data
    // there are many ways to get this data using jQuery (you can use the class or id also)
    var formData = {
        'website': target
    };

    // process the form
    updateValidation(formData);

    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();

}

function getValidation(data){
    $('input[name=website]').closest('.input-group').removeClass('has-error');
    $(".progress-bar").removeClass('btn-danger').removeClass('btn-success');

    $('.progress-bar').css('width', '10%').attr('aria-valuenow', 10);
    /* and display the numeric value */
    $(".progress-bar").html('validate url 10%');

    // process the form
    $.ajax({
        type        : 'GET', // define the type of HTTP verb we want to use (GET for our form)
        url         : '/validate', // the url where we want to GET
        data        : data, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
        .fail(function(result){
            $('input[name=website]').closest('.input-group').removeClass('has-success').addClass('has-error');

            $('.progress').addClass('hide');
        })
        // using the done promise callback
        .done(function(result) {
            console.log(result);
            $('input[name=website]').closest('.input-group').removeClass('has-error');
            // log data to the console so we can see
            if(result.redirect) window.location.replace(  result.redirect.host);
            if(!result.error) getReport(result.data);

        });
}


function updateValidation(data){
    $('input[name=website]').closest('.input-group').removeClass('has-error');
    $(".progress-bar").removeClass('btn-danger').removeClass('btn-success');

    $('.progress-bar').css('width', '10%').attr('aria-valuenow', 10);
    /* and display the numeric value */
    $(".progress-bar").html('validate url 10%');

    // process the form
    $.ajax({
        type        : 'GET', // define the type of HTTP verb we want to use (GET for our form)
        url         : '/validateOld', // the url where we want to GET
        data        : data, // our data object
        dataType    : 'json', // what type of data do we expect back from the server
        encode      : true
    })
        .fail(function(result){
            $('input[name=website]').closest('.input-group').removeClass('has-success').addClass('has-error');

            $('.progress').addClass('hide');
        })
        // using the done promise callback
        .done(function(result) {
            console.log(result);
            $('input[name=website]').closest('.input-group').removeClass('has-error');
            // log data to the console so we can see
            //if(result.redirect) window.location.replace(  result.redirect.host);
            if(!result.error) getReport(result.data);

        });
}

function getReport(data){

    // show progress bar
    $('.progress').removeClass('hide');



    Q.allSettled([
        $.get('/alexa', { })
            .done(function(data){response(data,'alexa');
                $.get('/google', {})
                    .done(function(data){response(data,'pagestats');
                        $.get('/semrush', {})
                            .done(function(data){response(data,'semrush');
                                $.get('/facebook', {})
                                    .done(function(data){response(data,'facebook');

                                        $.get('/pinterest', {})
                                            .done(function(data){response(data,'pinterest');
                                                $.get('/stumbleupon', {})
                                                    .done(function(data){response(data,'stumbleupon');
                                                        saveReport();
                                                    })
                                                    .fail(function(data){error(data,'stumbleupon')});

                                            })
                                            .fail(function(data){error(data,'pinterest')})

                                    })
                                    .fail(function(data){error(data,'facebook')})

                            })
                            .fail(function(data){error(data,'semrush')})
                    })
                    .fail(function(data){error(data,'pagestats')})

            })
            .fail(function(data){error(data,'alexa')})
    ])
        .then(function (results) {
            //saveReport();
        });

}

function response(data,message){
    console.info('Running '+message+' test')
    // get current width
    bar_width = $('.progress-bar').attr('aria-valuenow');
    bar_width = parseInt(bar_width) + Math.floor(Math.random() * 15) + 1 ;

    /* update the progress bar width */
    $('.progress-bar').css('width', bar_width+'%').attr('aria-valuenow', bar_width);
    /* and display the numeric value */
    $(".progress-bar").html('Running '+message+' test ' + bar_width + '%');

}

function error(data,message){
    console.warn('Error '+message)
    /* update the progress bar width */
    $(".progress-bar").html('Error saving website');
    $(".progress-bar").addClass('progress-bar-danger');


}

function saveReport() {
    console.info('Saving the report')

    $.ajax({
        type: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/save',
        success: function(data){
            // hide progress bar
            $('.progress').addClass('progress-bar-success');
            // redirect to site report page

            window.location.replace(data.url)
        },error:function(data){

            // hide progress bar
            $('.progress').addClass('btn-danger');
            // redirect to site report page

            $(".progress-bar").html('Error saving website');


        }});
}
