$(document).ready(function () {
    var analysis = {};

    $('span.stars').stars();

    // process the form
    $('form[name="report"]').submit(function (event) {
        event.preventDefault();
        var target = $(this).closest('form').find('input').val();
        var redirect = 0;
        startReport(target,redirect,event);
    });

    $('form[name="reportbar"]').submit(function (event) {
        event.preventDefault();
        var target = $(this).closest('form').find('input').val();
        var redirect = 1;
        startReport(target,redirect,event);
    });

    function startReport(target,redirect,event)
    {
        // show progress bar
        $('.progress').removeClass('hide');
        /* update the progress bar width */
        $('.progress-bar').css('width', '10%').attr('aria-valuenow', 10);
        /* and display the numeric value */
        $(".progress-bar").html('validate url 10%');

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'website'              : target,
            'redirect'             : redirect
        };

        // process the form
        getValidation(formData);

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();

    }

    function getValidation(data)
    {
        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'validate', // the url where we want to POST
            data        : data, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            encode      : true
        })
        .fail(function (result) {
            $('input[name=website]').closest('.input-group').removeClass('has-success').addClass('has-error');
            $('.progress').addClass('hide');
        })
        // using the done promise callback
        .done(function (result) {

            $('input[name=website]').closest('.input-group').removeClass('has-error');
            // log data to the console so we can see
            if (result.redirect) {
                window.location.replace(result.redirect.host);
            }
            if (!result.error) {
                getReport(result.data);
            }

        });
    }

    function getReport(data)
    {

        // show progress bar
        $('.progress').removeClass('hide');

        $.extend(analysis,data)
        var target = data.scheme + '://' + data.host;

        Q.allSettled([
                $.post('report', { website: target, report: 'pagerank' })
                    .done(function (data) {
                        response(data,'pagerank')})
                    .fail(function (data) {
                        error(data,'pagerank')}),
                $.post('report', { website: target, report: 'pagespeed' })
                    .done(function (data) {
                        response(data,'pagespeed')})
                    .fail(function (data) {
                        error(data,'pagespeed')}),
                $.post('report', { website: target, report: 'moz' })
                    .done(function (data) {
                        response(data,'moz')})
                    .fail(function (data) {
                        error(data,'moz')}),
                $.post('report', { website: data.host, report: 'geoip' })
                    .done(function (data) {
                        response(data,'geoip')})
                    .fail(function (data) {
                        error(data,'geoip')}),
                $.post('report', { website: target, report: 'safebrowsing' })
                    .done(function (data) {
                        response(data,'safebrowsing')})
                    .fail(function (data) {
                        error(data,'safebrowsing')}),
                $.post('report', { website: target, report: 'wot' })
                    .done(function (data) {
                        response(data,'wot')})
                    .fail(function (data) {
                        error(data,'wot')}),
                $.post('report', { website: target, report: 'semrush' })
                    .done(function (data) {
                        response(data,'semrush')})
                    .fail(function (data) {
                        error(data,'semrush')}),
                $.post('report', { website: target, report: 'sistrix' })
                    .done(function (data) {
                        response(data,'sistrix')})
                    .fail(function (data) {
                        error(data,'sistrix')}),
                $.post('report', { website: target, report: 'delicious' })
                    .done(function (data) {
                        response(data,'delicious')})
                    .fail(function (data) {
                        error(data,'delicious')}),
                $.post('report', { website: target, report: 'linkedin' })
                    .done(function (data) {
                        response(data,'linkedin')})
                    .fail(function (data) {
                        error(data,'linkedin')}),
                $.post('report', { website: target, report: 'digg' })
                    .done(function (data) {
                        response(data,'digg')})
                    .fail(function (data) {
                        error(data,'digg')}),
                $.post('report', { website: target, report: 'vkontakte' })
                    .done(function (data) {
                        response(data,'vkontakte')})
                    .fail(function (data) {
                        error(data,'vkontakte')}),
                $.post('report', { website: target, report: 'xing' })
                .done(function (data) {
                    response(data,'xing')})
                .fail(function (data) {
                    error(data,'xing')}),
                $.post('report', { website: target, report: 'pinterest' })
                    .done(function (data) {
                        response(data,'pinterest')})
                    .fail(function (data) {
                        error(data,'pinterest')}),
                $.post('report', { website: target, report: 'stumbleupon' })
                    .done(function (data) {
                        response(data,'stumbleupon')})
                    .fail(function (data) {
                        error(data,'stumbleupon')}),
                $.post('report', { website: target, report: 'facebook' })
                    .done(function (data) {
                        response(data,'facebook')})
                    .fail(function (data) {
                        error(data,'facebook')}),
                $.post('report', { website: target, report: 'twitter' })
                    .done(function (data) {
                        response(data,'twitter')})
                    .fail(function (data) {
                        error(data,'twitter')}),
                $.post('report', { website: target, report: 'googleplus' })
                    .done(function (data) {
                        response(data,'googleplus')})
                    .fail(function (data) {
                        error(data,'googleplus')}),
                $.post('report', { website: target, report: 'whois' })
                    .done(function (data) {
                        response(data,'whois')})
                    .fail(function (data) {
                        error(data,'whois')}),
                $.post('report', { website: target, report: 'alexa' })
                    .done(function (data) {
                        response(data,'alexa')})
                    .fail(function (data) {
                        error(data,'alexa')}),
                $.post('report', { website: target, report: 'alexagraph' })
                    .done(function (data) {
                        response(data,'alexagraph')})
                    .fail(function (data) {
                        error(data,'alexagraph')}),
                $.post('report', { website: data.host, report: 'dns' })
                    .done(function (data) {
                        response(data,'dns')})
                    .fail(function (data) {
                        error(data,'dns')}),
                $.post('report', { website: target, report: 'iplocation' })
                    .done(function (data) {
                        response(data,'iplocation')})
                    .fail(function (data) {
                        error(data,'iplocation')})
        ])
        .then(function (results) {
            saveReport();
        });

    }

    function response(data,message)
    {
        console.info('Running '+message+' test');
        // get current width
        bar_width = $('.progress-bar').attr('aria-valuenow');
        bar_width = parseInt(bar_width) + Math.floor(Math.random() * 3) + 1 ;

        /* update the progress bar width */
        $('.progress-bar').css('width', bar_width+'%').attr('aria-valuenow', bar_width);
        /* and display the numeric value */
        $(".progress-bar").html('Running '+message+' test ' + bar_width + '%');

        $.extend(analysis,data.data)
    }

    function error(data,message)
    {
        console.warn('Error '+message);
    }

    function saveReport()
    {
        console.info('Saving the report2');

        $.ajax({
            type: 'POST',
            url: 'save',
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify(analysis)
        })
        .success(function (data) {
            console.log(data)
            var analysis = {};

            // show progress bar
            $('.progress').addClass('hide');

            // redirect to site report page
            window.location.replace(data.url);
        });
    }
});

$.fn.stars = function () {
    return $(this).each(function () {
        // Get the value
        var val = parseFloat($(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
        // Create stars holder
        var $span = $('<span />').width(size);
        // Replace the numerical value with stars
        $(this).html($span);
    });
}
