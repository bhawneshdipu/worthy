Chart.defaults.global.elements.point.radius = 5;
Chart.defaults.global.elements.point.hitRadius = 50;
Chart.defaults.global.elements.point.hoverBorderWidth = 50;
var myAlexaChartTextAndRank1;
var myAlexaChartDelta1;
var myAlexaChartCountryRank1;

var myAlexaChartDailyUniqueVisits1;
var myAlexaChartDailyPageViews1;
var myAlexaChartIncomePerDay1;
var myAlexaChartEstimatedValue1;


var mySumrushChartRk1;
var mySumrushChartOr1;
var mySumrushChartOt1;
var mySumrushChartOc1;

var myResponseTimeChart1;
var myFacebookCommentChart1;
var myFacebookShareChart1;
var myDesktopSpeedScoreChart1;
var myMobileSpeedScoreChart1;

var myMobileUsabilityScoreChart1;
var myPinterestChart1;
var myStumbleuponChart1;




function createResponseChart1(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myResponseTimeChart1').html('');
    createResponseTimeChart1(labels,data);

}
function createFacebookChart1(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myFacebookCommentChart1').html('');
    $('#myFacebookShareChart1').html('');
    createFacebookShareChart1(labels,data);
    createFacebookCommentChart1(labels,data);

}

function createSpeedScoreChart1(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myDesktopSpeedScoreChart1').html('');
    $('#myMobileSpeedScoreChart1').html('');
    createDesktopSpeedScoreChart1(labels,data);
    createMobileSpeedScoreChart1(labels,data);

}
function createUsabilityScoreChart1(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myMobileUsabilityScoreChart1').html('');
    createMobileUsabilityScoreChart1(labels,data);

}
function createPinterestCountChart1(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myPinterestChart1').html('');
    createPinterestChart1(labels,data);

}

function createStumbleuponCountChart1(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myStumbleuponChart1').html('');
    createStumbleuponChart1(labels,data);

}




function createAlexaChart1(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#myAlexaChartTextAndRank1').html('');
    $('#myAlexaChartDelta1').html('');
    $('#myAlexaChartCountryRank1').html('');
    createAlexaChartTextAndRank1(labels,data);
    createAlexaChartDelta1(labels,data);
    createAlexaChartCountryRank1(labels,data);

    $('#myAlexaChartDailyUniqueVisits1').html('');
    $('#myAlexaChartDailyPageViews1').html('');
    $('#myAlexaChartIncomePerDay1').html('');
    $('#myAlexaChartEstimatedValue1').html('');

    createAlexaChartDailyUniqueVisits1(labels,data);
    createAlexaChartDailyPageViews1(labels,data);
    createAlexaChartIncomePerDay1(labels,data);
    createAlexaChartEstimatedValue1(labels,data);




}
function createSumrushChart1(labels,data) {
    data=JSON.parse(data);
    labels=JSON.parse(labels);
    $('#mySumrushChartRk1').html('');
    $('#mySumrushChartOr1').html('');
    $('#mySumrushChartOt1').html('');
    $('#mySumrushChartOc1').html('');

    createmySumrushChartRk1(labels,data);
    createmySumrushChartOr1(labels,data);
    createmySumrushChartOt1(labels,data);
    createmySumrushChartOc1(labels,data);



}



function getData1(days) {
    var labesl=null;
    var data=null;
    console.log($('#mywebsite1').val());
    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getAlexaXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getAlexaYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createAlexaChart1(xdata,ydata);
                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------Sumrush-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getSumrushXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getSumrushYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createSumrushChart1(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------Facebook-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYFacebookCount",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createFacebookChart1(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

    /*---speed score---*/



    /*-----------Facebook-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYSpeedScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createSpeedScoreChart1(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });




    /*-----------Mobile Usability-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYMobileUsablityScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createUsabilityScoreChart1(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });



    /*-----------Pinterest Usability-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYPinterest",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createPinterestCountChart1(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });





    /*-----------Stumbleupon -------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYStumbleupon",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createStumbleuponCountChart1(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });




    /*-----------Response TIme -------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYResponseTime",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    createResponseChart1(xdata,ydata);

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

}

function updateData1(days) {
    var labesl=null;
    var data=null;
    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getAlexaXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getAlexaYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myAlexaChartTextAndRank1.data.labels=xdata;
                    myAlexaChartTextAndRank1.data.datasets[0].data=ydata.rank;
                    myAlexaChartTextAndRank1.data.datasets[1].data=ydata.text;
                    myAlexaChartDelta1.data.datasets[0].data=ydata.delta;
                    myAlexaChartCountryRank1.data.datasets[0].data=ydata.country_rank;

                    myAlexaChartTextAndRank1.update();

                    myAlexaChartDelta1.update();
                    myAlexaChartCountryRank1.update();

                    myAlexaChartDailyUniqueVisits1.data.datasets[0].data=ydata.daily_unique_visits;
                    myAlexaChartDailyPageViews1.data.datasets[0].data=ydata.daily_page_views;
                    myAlexaChartIncomePerDay1.data.datasets[0].data=ydata.income_per_day;
                    myAlexaChartEstimatedValue1.data.datasets[0].data=ydata.estimated_value;


                    myAlexaChartDailyUniqueVisits1.data.labels=xdata;
                    myAlexaChartDailyPageViews1.data.labels=xdata;
                    myAlexaChartIncomePerDay1.data.labels=xdata;
                    myAlexaChartEstimatedValue1.data.labels=xdata;

                    myAlexaChartDailyUniqueVisits1.update();
                    myAlexaChartDailyPageViews1.update();
                    myAlexaChartIncomePerDay1.update();
                    myAlexaChartEstimatedValue1.update();







                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

    /*-----------Sumrush-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getSumrushXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getSumrushYData",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    mySumrushChartRk1.data.labels=xdata;
                    mySumrushChartRk1.data.datasets[0].data=ydata.Rk;
                    mySumrushChartRk1.update();

                    mySumrushChartOr1.data.labels=xdata;
                    mySumrushChartOr1.data.datasets[0].data=ydata.Or;
                    mySumrushChartOr1.update();

                    mySumrushChartOt1.data.labels=xdata;
                    mySumrushChartOt1.data.datasets[0].data=ydata.Ot;
                    mySumrushChartOt1.update();

                    mySumrushChartOc1.data.labels=xdata;
                    mySumrushChartOc1.data.datasets[0].data=ydata.Oc;
                    mySumrushChartOc1.update();

                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });




    /*-----------Facebook-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYFacebookCount",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myFacebookCommentChart1.data.labels=xdata;
                    myFacebookCommentChart1.data.datasets[0].data=ydata.comment;
                    myFacebookCommentChart1.update();

                    myFacebookShareChart1.data.labels=xdata;
                    myFacebookShareChart1.data.datasets[0].data=ydata.share;
                    myFacebookShareChart1.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });



    /*-----------Speed Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYSpeedScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myDesktopSpeedScoreChart1.data.labels=xdata;
                    myDesktopSpeedScoreChart1.data.datasets[0].data=ydata.desktop;
                    myDesktopSpeedScoreChart1.update();

                    myMobileSpeedScoreChart1.data.labels=xdata;
                    myMobileSpeedScoreChart1.data.datasets[0].data=ydata.mobile;
                    myMobileSpeedScoreChart1.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });






    /*-----------Usability Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYMobileUsablityScore",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myMobileUsabilityScoreChart1.data.labels=xdata;
                    myMobileUsabilityScoreChart1.data.datasets[0].data=ydata;
                    myMobileUsabilityScoreChart1.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });





    /*-----------Pinterest  Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYPinterest",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myPinterestChart1.data.labels=xdata;
                    myPinterestChart1.data.datasets[0].data=ydata;
                    myPinterestChart1.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------getYStumbleupon  Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYStumbleupon",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myStumbleuponChart1.data.labels=xdata;
                    myStumbleuponChart1.data.datasets[0].data=ydata;
                    myStumbleuponChart1.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });


    /*-----------getYResponseTime  Score-------------*/

    $.ajax({
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/api/1/getXData",
        data: {
            'days': days,
            'website':$('#mywebsite1').val()
        },
        success: function (xdata) {
            console.log(xdata);
            labels=xdata;
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getYResponseTime",
                data: {
                    'days':days,
                    'website':$('#mywebsite1').val()
                },
                success: function (ydata) {
                    console.log(ydata);
                    ydata=JSON.parse(ydata);
                    xdata=JSON.parse(xdata);
                    myResponseTimeChart1.data.labels=xdata;
                    myResponseTimeChart1.data.datasets[0].data=ydata;
                    myResponseTimeChart1.update();


                },
                error: function (res) {
                    console.log(res);
                    return null;
                }
            });
        },
        error: function (res) {
            console.log(res);
            return null;
        }
    });

}



$(document).ready(function(){
    getData1(30);
});
$('#days').on('change',function(){
    updateData1($(this).val());
});













/*********************chat js***************/

function createAlexaChartTextAndRank1(labels,data) {

    console.log("myAlexaChartTextAndRank1");
    console.log(labels);
    console.log(data);


    var ctx = document.getElementById("myAlexaChartTextAndRank1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartTextAndRank1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.rank,
                    label: "RANK",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                },
                {
                    data: data.text,
                    label: "TEXT",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black",
                        reverse:true
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}





function createAlexaChartDelta1(labels,data) {


    var ctx = document.getElementById("myAlexaChartDelta1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartDelta1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.delta,
                    label: "DELTA",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartCountryRank1(labels,data) {


    var ctx = document.getElementById("myAlexaChartCountryRank1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartCountryRank1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.country_rank,
                    label: "DELTA:"+data.country_name[0],
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}
/*=====================================================new Alexa {age Stats===================*/


function createAlexaChartDailyUniqueVisits1(labels,data) {


    var ctx = document.getElementById("myAlexaChartDailyUniqueVisits1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartDailyUniqueVisits1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.daily_unique_visits,
                    label: "DailyUniqueVisits",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartDailyPageViews1(labels,data) {


    var ctx = document.getElementById("myAlexaChartDailyPageViews1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartDailyPageViews1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.daily_page_views,
                    label: "DailyPageViews",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartIncomePerDay1(labels,data) {


    var ctx = document.getElementById("myAlexaChartIncomePerDay1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartIncomePerDay1= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.income_per_day,
                    label: "IncomePerDay",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createAlexaChartEstimatedValue1(labels,data) {


    var ctx = document.getElementById("myAlexaChartEstimatedValue1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myAlexaChartEstimatedValue1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.estimated_value,
                    label: "EstimatedValue",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



/*================================================================================================*/


function createmySumrushChartRk1(labels,data) {


    var ctx = document.getElementById("mySumrushChartRk1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartRk1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Rk,
                    label: "RANK",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createmySumrushChartOr1(labels,data) {


    var ctx = document.getElementById("mySumrushChartOr1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartOr1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Or,
                    label: "Or",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createmySumrushChartOt1(labels,data) {


    var ctx = document.getElementById("mySumrushChartOt1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartOt1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Ot,
                    label: "Ot",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


function createmySumrushChartOc1(labels,data) {


    var ctx = document.getElementById("mySumrushChartOc1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    mySumrushChartOc1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [
                {
                    data: data.Oc,
                    label: "Oc",
                    borderColor: "#ff0000",
                    fill: false,
                    backgroundColor: "#ff91a7"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}

/*-------Facebook--------*/


function createFacebookShareChart1(labels,data) {


    var ctx = document.getElementById("myFacebookShareChart1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myFacebookShareChart1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.share,
                    label: "Facebook Share",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



function createFacebookCommentChart1(labels,data) {


    var ctx = document.getElementById("myFacebookCommentChart1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myFacebookCommentChart1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.comment,
                    label: "Facebook Comment",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



/*---------google Speed socre--*/



function createFacebookShareChart1(labels,data) {


    var ctx = document.getElementById("myFacebookShareChart1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myFacebookShareChart1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.share,
                    label: "Facebook Share",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


/*---speed score --*/

/*---------google Speed socre--*/



function createDesktopSpeedScoreChart1(labels,data) {


    var ctx = document.getElementById("myDesktopSpeedScoreChart1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myDesktopSpeedScoreChart1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.desktop,
                    label: "Desktop Speed",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



function createMobileSpeedScoreChart1(labels,data) {


    var ctx = document.getElementById("myMobileSpeedScoreChart1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myMobileSpeedScoreChart1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data.mobile,
                    label: "Mobile Speed",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}






function createMobileUsabilityScoreChart1(labels,data) {


    var ctx = document.getElementById("myMobileUsabilityScoreChart1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myMobileUsabilityScoreChart1 = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Mobile Usability Score",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}





function createPinterestChart1(labels,data) {


    var ctx = document.getElementById("myPinterestChart1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myPinterestChart1= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Pinterest ",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}




function createStumbleuponChart1(labels,data) {


    var ctx = document.getElementById("myStumbleuponChart1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myStumbleuponChart1= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Stumbleupon",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}



function createResponseTimeChart1(labels,data) {


    var ctx = document.getElementById("myResponseTimeChart1").getContext("2d");
    ctx.canvas.width = 450;
    ctx.canvas.height = 250;
    myResponseTimeChart1= new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [

                {
                    data: data,
                    label: "Response Time",
                    borderColor: "#2bb3ff",
                    fill: false,
                    backgroundColor: "#84ddff"
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false,
                        fontSize: 15,
                        fontColor: "black"
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontSize: 15,
                        display: false

                    }
                }]


            },
            elements: {
                line: {
                    tension: 0 // disables bezier curves
                }
            },
            tooltips: {
                bodyFontSize: 18,
                titleFontSize: 15,
                bodyFontStyle: "bold",
                caretPadding: 2,
                caretSize: 10,
                cornerRadius: 15,
                xPadding: 20,
                yPadding: 10
            },
            legend: {
                display: false
            }

        }
    });
}


