<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('host')->unique();
            $table->string('ip');
            $table->text('title');
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->text('response_time')->nullabe();
            $table->text('screenshot')->nullable();

            // Alexa
            $table->string('alexa_rank')->default('N/A');
            $table->string('alexa_text')->default('N/A');
            $table->string('alexa_delta')->default('N/A');
            // Alexa country rank
            $table->string('alexa_country_code')->default('N/A');
            $table->string('alexa_country_rank')->default('N/A');
            $table->string('alexa_country_name')->default('N/A');
            // Semrush graph

            $table->string('daily_unique_visits')->default('0');
            $table->string('daily_page_views')->default('0');
            $table->string('income_per_day')->default('0');
            $table->string('estimated_value')->default('0');

            $table->text('semrush_rank')->nullable();

            // Social shares
            $table->string('facebook_comment_count')->default(0);
            $table->string('facebook_share_count')->default(0);

            $table->string('stumbleupon')->default(0);

            $table->string('pinterest')->default(0);
            //Google Api
            $table->string('desktop_speed_score')->default(0);

            $table->string('mobile_speed_score')->default(0);
            $table->string('mobile_usablity_score')->default(0);
            $table->text('mobile_page_stats')->nullable();
            $table->text('desktop_page_stats')->nullable();
            $table->boolean('hide')->default(false);
            $table->timestamp('date')->useCurrent();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websites');
    }
}
