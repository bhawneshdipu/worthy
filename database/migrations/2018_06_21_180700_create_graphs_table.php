<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraphsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('graphs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('host');
            $table->string('ip');
            $table->text('title');
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->text('response_time');
            $table->text('screenshot')->nullable();

            // Alexa
            $table->string('alexa_rank')->default('0');
            $table->string('alexa_text')->default('0');
            $table->string('alexa_delta')->default('0');

            $table->string('daily_unique_visits')->default('0');
            $table->string('daily_page_views')->default('0');
            $table->string('income_per_day')->default('0');
            $table->string('estimated_value')->default('0');

            // Alexa country rank
            $table->string('alexa_country_code')->default('N/A');
            $table->string('alexa_country_rank')->default('N/A');
            $table->string('alexa_country_name')->default('N/A');
            // Semrush graph
            $table->text('semrush_rank')->nullable();

            $table->string('semrush_Rk')->default(0);
            $table->string('semrush_Or')->default(0);
            $table->string('semrush_Ot')->default(0);
            $table->string('semrush_Oc')->default(0);
            // Social shares
            $table->string('facebook_comment_count')->default(0);
            $table->string('facebook_share_count')->default(0);
            $table->string('stumbleupon')->default(0);
            $table->string('pinterest')->default(0);
            //Google Api
            $table->string('desktop_speed_score')->nullable();
            $table->string('mobile_speed_score')->nullable();
            $table->string('mobile_usablity_score')->nullable();
            $table->text('mobile_page_stats')->nullable();
            $table->text('desktop_page_stats')->nullable();
            $table->boolean('hide')->default(0);
            $table->timestamp('date')->useCurrent();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('graphs');
    }
}
