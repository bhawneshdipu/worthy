<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $settings=new \App\Setting();
        $settings->name='Bad Words';
        $settings->handle='bad_words';
        $settings->value='ahole,anus,ass,asshole,assholes,assholz,asswipe,azzhole,bassterds,bastard,bastards,bastardz,basterds,basterdz,bitch,bitches,BlowJob,boffing,butthole,buttwipe,cock,cocks,cock,cockhead,cock-head,cocks,cum,cunt,cunts,dick,dildo,enema,fag,fag1t,faget,fagg1t,faggit,faggot,fagit,fags,fagz,faig,faigs,fuck,fucker,fuckin,fucking,fucks';
        $settings->description='Bad Words';
        $settings->default='ahole,anus,ass,asshole,assholes,assholz,asswipe,azzhole,bassterds,bastard,bastards,bastardz,basterds,basterdz,bitch,bitches,BlowJob,boffing,butthole,buttwipe,cock,cocks,cock,cockhead,cock-head,cocks,cum,cunt,cunts,dick,dildo,enema,fag,fag1t,faget,fagg1t,faggit,faggot,fagit,fags,fagz,faig,faigs,fuck,fucker,fuckin,fucking,fucks';

        $settings->save();
        $settings=new \App\Setting();

        $settings->name='Bad Words2';
        $settings->handle='bad_words_2';
        $settings->value='ahole,anus,ass,asshole,assholes,assholz,asswipe,azzhole,bassterds,bastard,bastards,bastardz,basterds,basterdz,bitch,bitches,BlowJob,boffing,butthole,buttwipe,cock,cocks,cock,cockhead,cock-head,cocks,cum,cunt,cunts,dick,dildo,enema,fag,fag1t,faget,fagg1t,faggit,faggot,fagit,fags,fagz,faig,faigs,fuck,fucker,fuckin,fucking,fucks';
        $settings->description='Bad Words_2';
        $settings->default='ahole,anus,ass,asshole,assholes,assholz,asswipe,azzhole,bassterds,bastard,bastards,bastardz,basterds,basterdz,bitch,bitches,BlowJob,boffing,butthole,buttwipe,cock,cocks,cock,cockhead,cock-head,cocks,cum,cunt,cunts,dick,dildo,enema,fag,fag1t,faget,fagg1t,faggit,faggot,fagit,fags,fagz,faig,faigs,fuck,fucker,fuckin,fucking,fucks';

        $settings->save();

        $settings=new \App\Setting();

        $settings->name='Slot 1';
        $settings->handle='slot_1';
        $settings->value='Slot 1';
        $settings->description='Slot 1';
        $settings->default='Slot 1';

        $settings->save();

        $settings=new \App\Setting();

        $settings->name='Slot 2';
        $settings->handle='slot_2';
        $settings->value='Slot 2';
        $settings->description='Slot 2';
        $settings->default='Slot 2';

        $settings->save();


    }
}
