
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Worthy - Powerful dashboard</title>

    <!-- Latest Bootstrap compiled and minified CSS -->
    <link rel="stylesheet" href="/css/fontawesome.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/base.css">
    <link rel="stylesheet" href="/css/themes/cerulean.css">
    <link rel="stylesheet" href="/css/helper/themes/cerulean.css">
    <link rel="stylesheet" href="/css/bootstrap-select.min.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .navbar-fixed-top{max-height: 50px;};
        .results{max-height:20px;min-height:20px;overflow-y: auto;padding:0px 7px 0px 5px;max-height:100%;margin-left:4px!important;margin-bottom:0;background:#fff;bottom:/*126px*/92px;}
        .results li {border: none;list-style: none;color:#909090;font-size: 13px;padding:7px;}
        .results li  + li {border-top:1px dotted #cecece;}
        ul.results li:hover,li:hover {color:#f5821f;background:#98cbe8;}
        ul.results li{background: ghostwhite;}
    </style>

</head>
<body>



<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://gtools.bid">Worthy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <form method="post" name="reportbar" class="navbar-form">
                    <div class="form-group">
                        <input type="text" name="website" id="website" placeholder="Url" class="form-control" >
                        <ul class="results" style="margin-left: -2.7em; width: 100%;min-height: 0px;max-height: 100px;z-index:100; " id="website-list">

                        </ul>

                    </div>
                    <button type="submit" class="btn btn-success">Go</button>
                </form>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://gtools.bid/top">Top</a></li>
                <li><a href="http://gtools.bid/recent">Recent</a></li>
            </ul>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Your Page Content Here -->

<div class="progress hide">
    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
    </div>
</div>

@if(isset($website))
<div class="container padtop">
    <!-- row of columns -->
    <div class="row">

        <!-- Left column -->
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-10"><h2>Web analysis of {!! $website->host !!}<a href="{!! $website->host !!}"><i class="fa fa-external-link"></i></a></h2></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <img src="{!! $website->screenshot !!}" class="pull-left">
                </div>
                <div class="col-md-6">
                    <h3> {!! $website->host !!}</h3>
                    <p>

                    <form id="updatereport" method="" name="updatereport" class="text-center" onsubmit="event.preventDefault(); updateMyReport();">
                        <input type="hidden" name="website" value="{!! $website->host !!}" id="update_website" required>
                        @php
                            $date=new \DateTime();
                            $updated_day=new \DateTime($website->date);
                            $past_day=$date->modify('-1 day');
                        @endphp
                        @if($updated_day<=$past_day)
                        <button class="btn btn-primary btn-xs" role="button">
                            Update
                            <i class="fa fa-refresh"></i>
                        </button>
                            @else
                            <button class="btn btn-primary btn-xs" role="button" disabled="disabled">
                                Update
                                <i class="fa fa-refresh"></i>
                            </button>
                            @endif
                        <a class="btn-primary btn btn-xs" onclick="window.open('/{!! $website->host !!}/{!! $website->host !!}','_blank');" target="_blank" >Compare</a>
                    </form>
                    @if($website->hide==false)
                        <p>{!! $slot1->value !!}</p>
                    @else
                        <p>{!! $slot2->value !!}</p>
                    @endif

                </div>
            </div>




            <!-- metatag -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Metatag</div>
                        <div class="">
                            <table class="table table-hover">
                                <tr>
                                    <td class="col-md-6"><i class="fa fa-file-o"></i> Title</td>
                                    <td class="col-md-6">{!! $website->title !!}</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-file-o"></i> Description</td>
                                    <td>{!! $website->description !!}</td>
                                </tr>

                                <tr>
                                    <td><i class="fa fa-file-o"></i> Keywords</td>
                                    <td>{!! $website->keywords !!}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Desktop Page Stats</div>
                        <div class="" id="desktop_page_stats">

                            @php
                            $json=$website->desktop_page_stats;
                            $json_array=json_decode($json);
                            @endphp
                            <table class="table table-striped">
                                @if(isset($json_array) and $json_array!=null and $json_array!='')

                                @foreach($json_array as $key =>$value)
                                    <tr><td>{!! $key !!}</td><td>{!! $value !!}</td></tr>
                                @endforeach
                                @endif
                            </table>

                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">mobile Page Stats</div>
                        <div class="" id="mobile_page_stats">

                            @php
                                $json=$website->mobile_page_stats;
                                $json_array=json_decode($json);
                            @endphp
                            <table class="table table-striped">
                                @if(isset($json_array) and $json_array!=null and $json_array!='')
                                @foreach($json_array as $key =>$value)
                                    <tr><td>{!! $key !!}</td><td>{!! $value !!}</td></tr>
                                @endforeach
                                    @endif

                            </table>

                        </div>
                    </div>
                </div>
            </div><!-- ./ metatag -->

            <!-- Graphs -->

            @if(isset($website))
                <div class="row col-md-12">




                    <div class="panel panel-default">
                        <div class="panel-heading">Website TEXT AND RANK </div>
                        <div class="panel-body">

                            <canvas id="myAlexaChartTextAndRank" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from Alexa
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>



                    <div class="panel panel-default">
                        <div class="panel-heading">Website Alexa Delta  </div>
                        <div class="panel-body">

                            <canvas id="myAlexaChartDelta" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from Alexa Delta
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Website Alexa COUNTRY RANK</div>
                        <div class="panel-body">

                            <canvas id="myAlexaChartCountryRank" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from Alexa
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Website daily_unique_visits  </div>
                        <div class="panel-body">

                            <canvas id="myAlexaChartDailyUniqueVisits" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from daily_unique_visits
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Website daily_page_views</div>
                        <div class="panel-body">

                            <canvas id="myAlexaChartDailyPageViews" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from daily_page_views
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Website income_per_day</div>
                        <div class="panel-body">

                            <canvas id="myAlexaChartIncomePerDay" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from income_per_day
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Website estimated_value</div>
                        <div class="panel-body">

                            <canvas id="myAlexaChartEstimatedValue" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from estimated_value
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>

                        </div>
                    </div>






                    <div class="panel panel-default">
                        <div class="panel-heading">Website RK </div>
                        <div class="panel-body">

                            <canvas id="mySumrushChartRk" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from Sumrush
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Website OR </div>
                        <div class="panel-body">

                            <canvas id="mySumrushChartOr" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from Sumrush
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Website OT </div>
                        <div class="panel-body">

                            <canvas id="mySumrushChartOt" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from Sumrush
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Website Oc
                            <div class="panel-body">

                                <canvas id="mySumrushChartOc" ></canvas>

                            </div>
                            <div class="panel-footer">
                                Website data from Sumrush
                                <div class="col-sm-3  pull-right">
                                    <select name="days" id="days" class="select form-control">
                                        <option value="7">1 Week</option>
                                        <option value="30" selected="selected">1 Month</option>
                                        <option value="60">2 Month</option>
                                        <option value="90">3 Month</option>
                                        <option value="182">6 Month</option>
                                        <option value="365">1 Year</option>

                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">Response Time </div>
                        <div class="panel-body">

                            <canvas id="myResponseTimeChart" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from myResponseTimeChart
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">Facebook Comment </div>
                        <div class="panel-body">

                            <canvas id="myFacebookCommentChart" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from Facebook
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">myFacebookShareChart </div>
                        <div class="panel-body">

                            <canvas id="myFacebookShareChart" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from Sumrush
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">Desktop SpeedScore </div>
                        <div class="panel-body">

                            <canvas id="myDesktopSpeedScoreChart" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from myDesktopSpeedScore
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Mobile SpeedScore </div>
                        <div class="panel-body">

                            <canvas id="myMobileSpeedScoreChart" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from myMobileSpeedScore
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Mobile Usability Score </div>
                        <div class="panel-body">

                            <canvas id="myMobileUsabilityScoreChart" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from myDesktopSpeedScore
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>




                    <div class="panel panel-default">
                        <div class="panel-heading">Pinterest </div>
                        <div class="panel-body">

                            <canvas id="myPinterestChart" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from getYPinterest
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">Stumbleupon Score </div>
                        <div class="panel-body">

                            <canvas id="myStumbleuponChart" ></canvas>

                        </div>
                        <div class="panel-footer">
                            Website data from myStumbleuponChart
                            <div class="col-sm-3  pull-right">
                                <select name="days" id="days" class="select form-control">
                                    <option value="7">1 Week</option>
                                    <option value="30" selected="selected">1 Month</option>
                                    <option value="60">2 Month</option>
                                    <option value="90">3 Month</option>
                                    <option value="182">6 Month</option>
                                    <option value="365">1 Year</option>

                                </select>
                            </div>
                        </div>
                    </div>



                </div>
            @endif






</div>



        <div class="col-md-4">


            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-clock"></i> Similar ranked
                        </div>
                        <div class="panel-body">

                            @if(isset($similar))
                                @foreach($similar as $web)
                            <div class="row">
                                <div class="col-md-12 card-header">
                                    <a href="{!! $web->host !!}">{!! $web->host !!}</a>
                                    <a href="{!! $web->host !!}"><i class="fa fa-external-link"></i></a>
                                </div>
                                <div class="col-md-12 card-body">
                                    <div class="col-md-7">
                                        <a href="{!! $web->host !!}">
                                            <img alt="{!! $web->host !!}" src="{!! $web->screenshot !!}" style="max-width:100%">
                                        </a>
                                    </div>
                                    <div class="col-md-5">
                                        <p>
                                        {!! $web->description !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock"></i> Last inserted
                        </div>
                        <div class="panel-body">
                            @if(isset($recent))
                                @foreach($recent as $web)

                            <div class="row">
                                <div class="col-md-12 card-header">
                                    <a href="{!! $web->host !!}">{!! $web->host !!}</a>
                                    <a href="{!! $web->host !!}"><i class="fa fa-external-link"></i></a>
                                </div>
                                <div class="col-md-12 card-body">
                                    <div class="col-md-7">
                                        <a href="{!! $web->host !!}">
                                            <img alt="{!! $web->host !!}" src="{!! $web->screenshot !!}" style="max-width:100%">
                                        </a>
                                    </div>
                                    <div class="col-md-5">
                                        <p>
                                            {!! $web->description !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                                @endforeach
                                @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <hr>
    </div>
</div>

@endif

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><p>All rights reserved - 2014</p></div>
            </div>
        </div>
    </footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- Latest compiled and minified JavaScript -->
<script src="/js/bootstrap.min.js"></script>

<script src="/js/Chart.bundle.min.js" ></script>
<script src="/js/bootstrap-select.min.js" ></script>

<script src="/js/q.js"></script>

<script src="/js/report.js"></script>
<script src="/js/graph.js"></script>
<input type="hidden" id="mywebsite" value='@if(isset($website)){!! $website->host!!}@endif'/>

<script>
    $('#website').on('keyup',function(){

        $('#website-list').html('');

        if($('#website').val()==null || $('#website').val().length==0){
            return;
        }

        $.ajax({
            type: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/api/1/getweblist",
            data: {
                'keyword': $('#website').val()
            },
            success: function (res) {
                console.log(res);
                res.forEach(function(value,key){
                    $('#website-list').append('<li value="'+value+'">'+value+'</li>');
                });

            },
            error:function(res){
                console.log(res);
            }
        });

    });

    $(document).on('click','ul.results > li',function(){

        $(this).parent().parent().find('input').val($(this).attr('value'));
        $(this).parent('ul').html('');
    });
</script>

</body>
</html>
