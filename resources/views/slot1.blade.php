<div class="col-md-3 col-sm-6 hero-feature">
    <div class="thumbnail">
        <img src='{!! $website->screenshot !!}'>
        <div class="caption">
            <h4>{!! $website->host !!}</h4>
            <p>
                <a href="{!! env('APP_DOMAIN') !!}/{!! $website->host !!}" class="btn btn-primary">Check review</a> <a href="#" class="btn btn-default">Visit site <i class="fa fa"></i></a>
            </p>
        </div>
    </div>
</div>