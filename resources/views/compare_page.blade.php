
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Worthy - Powerful dashboard</title>

    <!-- Latest Bootstrap compiled and minified CSS -->
    <link rel="stylesheet" href="/css/fontawesome.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/base.css">
    <link rel="stylesheet" href="/css/themes/cerulean.css">
    <link rel="stylesheet" href="/css/helper/themes/cerulean.css">
    <link rel="stylesheet" href="/css/bootstrap-select.min.css">

    <!--<link rel="stylesheet" href="http://gtools.bid/css/themes/cerulean.css">-->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
    .results { max-height:20px;min-height:20px;overflow-y: auto;padding:0px 7px 0px 5px;max-height:100%;margin-left:4px!important;margin-bottom:0;background:#fff;bottom:/*126px*/92px;}
    .results li {border: none;list-style: none;color:#909090;font-size: 13px;padding:7px;}
    .results li  + li {border-top:1px dotted #cecece;}
    ul.results li:hover,li:hover {color:#f5821f;background:#98cbe8;}

</style>
<style>
        .navbar-fixed-top{max-height: 50px;};
        ul.results li{background: ghostwhite;}
</style>

</head>
<body>



<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://gtools.bid">Worthy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <form method="post" name="reportbar" class="navbar-form">
                    <div class="form-group">
                        <input type="text" name="website" id="website" placeholder="Url" class="form-control" >
                        <ul class="results" style="margin-left: -2.7em; width: 100%;min-height: 0px;max-height: 100px;z-index:100; " id="website-list">

                        </ul>

                    </div>
                    <button type="submit" class="btn btn-success">Go</button>
                </form>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://gtools.bid/top">Top</a></li>
                <li><a href="http://gtools.bid/recent">Recent</a></li>
            </ul>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Your Page Content Here -->

<div class="progress hide">
    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
    </div>
</div>
<div class="clearfix col-md-offset-4">
    <h2>Compare two websites</h2>
</div>
@if(isset($website1) && isset($website2))
<div class="col-lg-12 col-md-12">
    <div class="col-md-6">
        <input class="form-control" name="myformwebsite1" id="myformwebsite1" placeholder="Enter website1" value="{!! $website1->host !!}">
        <ul class="results" style="margin-left: -2.7em; width: 100%;min-height: 0px;max-height: 100px;" id="website1-list">

        </ul>
    </div>
    <div class="col-md-6">
        <input class="form-control" name="myformwebsite2" id="myformwebsite2" placeholder="Enter Website 2" value="{!! $website2->host !!}">
        <ul class="results" style="margin-left: -2.7em; width: 100%;min-height: 0px;max-height: 100px;" id="website2-list">

        </ul>

    </div>
@else
        <div class="col-md-6">
            <input class="form-control" name="myformwebsite1" id="myformwebsite1" placeholder="Enter website1" value="">
            <ul class="results" style="margin-left: -2.7em; width: 100%;min-height: 0px;max-height: 100px;" id="website1-list">

            </ul>
        </div>
        <div class="col-md-6">
            <input class="form-control" name="myformwebsite2" id="myformwebsite2" placeholder="Enter Website 2" value="">
            <ul class="results" style="margin-left: -2.7em; width: 100%;min-height: 0px;max-height: 100px;" id="website2-list">

            </ul>

        </div>
    @endif
    <div class="clearfix">&nbsp;</div>
    <div class="col-md-6 col-md-offset-3 ">
        <a class="btn btn-block btn-primary" onclick="compareWebsites();"> Compare</a>
    </div>
</div>

<div class="col-lg-12 col-md-12">

    @if(isset($website1) && isset($website2))

        <div class="container padtop">
            <!-- row of columns -->
            <div class="row">

                <!-- Left column -->
                <div class="col-md-12">
                    <div class="col-md-6"><h2>Web analysis of {!! $website1->host !!}<a href="{!! $website1->host !!}"><i class="fa fa-external-link"></i></a></h2></div>
                    <div class="col-md-6"><h2>Web analysis of {!! $website2->host !!}<a href="{!! $website2->host !!}"><i class="fa fa-external-link"></i></a></h2></div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-6">
                        <img src="{!! $website1->screenshot !!}" class="pull-left">
                    </div>
                    <div class="col-md-6">
                        <img src="{!! $website2->screenshot !!}" class="pull-left">
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-6">
                        <h3> {!! $website1->host !!}</h3>
                        @if($website1->hide==false)
                            <p>{!! $slot1->value !!}</p>
                        @else
                            <p>{!! $slot2->value !!}</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <h3> {!! $website2->host !!}</h3>
                        @if($website2->hide==false)
                            <p>{!! $slot1->value !!}</p>
                        @else
                            <p>{!! $slot2->value !!}</p>
                        @endif
                    </div>

                </div>
                <!-- metatag -->
                <div class="row col-md-12">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Metatag</div>
                            <div class="">
                                <table class="table table-hover">
                                    <tr>
                                        <td class="col-sm-6"><i class="fa fa-file-o"></i> Title</td>
                                        <td class="col-sm-6">{!! $website1->title !!}</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-file-o"></i> Description</td>
                                        <td>{!! $website1->description !!}</td>
                                    </tr>

                                    <tr>
                                        <td><i class="fa fa-file-o"></i> Keywords</td>
                                        <td>{!! $website1->keywords !!}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Metatag</div>
                            <div class="">
                                <table class="table table-hover">
                                    <tr>
                                        <td class="col-sm-6"><i class="fa fa-file-o"></i> Title</td>
                                        <td class="col-sm-6">{!! $website2->title !!}</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-file-o"></i> Description</td>
                                        <td>{!! $website2->description !!}</td>
                                    </tr>

                                    <tr>
                                        <td><i class="fa fa-file-o"></i> Keywords</td>
                                        <td>{!! $website2->keywords !!}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Desktop Page Stats</div>
                    <div class="" id="desktop_page_stats1">

                        @php
                            $json=$website1->desktop_page_stats;
                            $json_array=json_decode($json);
                        @endphp
                        <table class="table table-striped">
                            @if(isset($json_array) and $json_array!=null and $json_array!='')

                                @foreach($json_array as $key =>$value)
                                    <tr><td>{!! $key !!}</td><td>{!! $value !!}</td></tr>
                                @endforeach
                            @endif
                        </table>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Desktop Page Stats</div>
                    <div class="" id="desktop_page_stats2">

                        @php
                            $json=$website2->desktop_page_stats;
                            $json_array=json_decode($json);
                        @endphp
                        <table class="table table-striped">
                            @if(isset($json_array) and $json_array!=null and $json_array!='')

                                @foreach($json_array as $key =>$value)
                                    <tr><td>{!! $key !!}</td><td>{!! $value !!}</td></tr>
                                @endforeach
                            @endif
                        </table>

                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Mobile Page Stats</div>
                    <div class="" id="mobile_page_stats1">

                        @php
                            $json=$website1->mobile_page_stats;
                            $json_array=json_decode($json);
                        @endphp
                        <table class="table table-striped">
                            @if(isset($json_array) and $json_array!=null and $json_array!='')

                                @foreach($json_array as $key =>$value)
                                    <tr><td>{!! $key !!}</td><td>{!! $value !!}</td></tr>
                                @endforeach
                            @endif
                        </table>

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Mobile Page Stats</div>
                    <div class="" id="mobile_page_stats2">

                        @php
                            $json=$website2->mobile_page_stats;
                            $json_array=json_decode($json);
                        @endphp
                        <table class="table table-striped">
                            @if(isset($json_array) and $json_array!=null and $json_array!='')

                                @foreach($json_array as $key =>$value)
                                    <tr><td>{!! $key !!}</td><td>{!! $value !!}</td></tr>
                                @endforeach
                            @endif
                        </table>

                    </div>
                </div>
            </div>

        </div>

    @endif
<!-- Graphs -->

    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website TEXT AND RANK </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartTextAndRank1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website TEXT AND RANK </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartTextAndRank2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website DELATA </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartDelta1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website DELTA </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartDelta2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website COUNTRY RANK </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartCountryRank1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website COUNTRY RANK </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartCountryRank2" ></canvas>
                    </div>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website DAILY UNIQUE VISITS </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartDailyUniqueVisits1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website DAILY UNIQIE VISITS </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartDailyUniqueVisits2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website DAILY PAGE VIEW </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartDailyPageViews1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website DAILY  PAGE VIEW </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartDailyPageViews2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website INCOME PER DAY </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartIncomePerDay1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website INCOME PER DAY </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartIncomePerDay2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website ESTIMATED VALUE </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartEstimatedValue1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website ESTIMATED VALUE </div>
                    <div class="panel-body">
                        <canvas id="myAlexaChartEstimatedValue2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website SUMRUSK RK  </div>
                    <div class="panel-body">
                        <canvas id="mySumrushChartRk1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website SUMRUSH RANK </div>
                    <div class="panel-body">
                        <canvas id="mySumrushChartRk2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website OR </div>
                    <div class="panel-body">
                        <canvas id="mySumrushChartOr1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website OR </div>
                    <div class="panel-body">
                        <canvas id="mySumrushChartOr2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website OT </div>
                    <div class="panel-body">
                        <canvas id="mySumrushChartOt1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website OT </div>
                    <div class="panel-body">
                        <canvas id="mySumrushChartOt2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website OC </div>
                    <div class="panel-body">
                        <canvas id="mySumrushChartOc1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website OC </div>
                    <div class="panel-body">
                        <canvas id="mySumrushChartOc2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website RESPONSE TIME </div>
                    <div class="panel-body">
                        <canvas id="myResponseTimeChart1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website RESPONSE TIME </div>
                    <div class="panel-body">
                        <canvas id="myResponseTimeChart2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if(isset($website2) && isset($website1))
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website FACEBOOK COMMENTS </div>
                    <div class="panel-body">
                        <canvas id="myFacebookCommentChart1" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Website FACEBOOK COMMENTS </div>
                    <div class="panel-body">
                        <canvas id="myFacebookCommentChart2" ></canvas>
                    </div>
                    <div class="panel-footer">
                        Website data from Alexa
                        <div class="col-sm-3  pull-right">
                            <select name="days" id="days" class="select form-control">
                                <option value="7">1 Week</option>
                                <option value="30" selected="selected">1 Month</option>
                                <option value="60">2 Month</option>
                                <option value="90">3 Month</option>
                                <option value="182">6 Month</option>
                                <option value="365">1 Year</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@if(isset($website2) && isset($website1))
    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website FB SHARE </div>
                <div class="panel-body">
                    <canvas id="myFacebookShareChart1" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website FB SHARE</div>
                <div class="panel-body">
                    <canvas id="myFacebookShareChart2" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif


@if(isset($website2) && isset($website1))
    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website DESKTOP SPEED SCORE</div>
                <div class="panel-body">
                    <canvas id="myDesktopSpeedScoreChart1" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website DESKTOP SPEED SCORE</div>
                <div class="panel-body">
                    <canvas id="myDesktopSpeedScoreChart2" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if(isset($website2) && isset($website1))
    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website MOBILE  SPEED SCORE</div>
                <div class="panel-body">
                    <canvas id="myMobileSpeedScoreChart1" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website MOBILE SPEED SCORE</div>
                <div class="panel-body">
                    <canvas id="myMobileSpeedScoreChart2" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

@if(isset($website2) && isset($website1))
    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website MOBILE  Usability Score</div>
                <div class="panel-body">
                    <canvas id="myMobileUsabilityScoreChart1" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website MOBILE  Usability Score</div>
                <div class="panel-body">
                    <canvas id="myMobileUsabilityScoreChart2" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if(isset($website2) && isset($website1))
    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website myPinterest</div>
                <div class="panel-body">
                    <canvas id="myPinterestChart1" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website RESPONSE TIME </div>
                <div class="panel-body">
                    <canvas id="myPinterestChart2" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

@if(isset($website2) && isset($website1))
    <div class="row col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website myStumbleupon</div>
                <div class="panel-body">
                    <canvas id="myStumbleuponChart1" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Website myStumbleupon </div>
                <div class="panel-body">
                    <canvas id="myStumbleuponChart2" ></canvas>
                </div>
                <div class="panel-footer">
                    Website data from Alexa
                    <div class="col-sm-3  pull-right">
                        <select name="days" id="days" class="select form-control">
                            <option value="7">1 Week</option>
                            <option value="30" selected="selected">1 Month</option>
                            <option value="60">2 Month</option>
                            <option value="90">3 Month</option>
                            <option value="182">6 Month</option>
                            <option value="365">1 Year</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif




    </div>



<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><p>All rights reserved - 2014</p></div>
        </div>
    </div>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery.min.js"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<!-- Latest compiled and minified JavaScript -->
<script src="/js/bootstrap.min.js"></script>

<script src="/js/Chart.bundle.min.js" ></script>
<script src="/js/q.js"></script>

<script src="/js/report.js"></script>
<script src="/js/graph2.js"></script>

<script src="/js/graph1.js"></script>
<script src="/js/bootstrap-select.min.js" ></script>

<input type="hidden" id="mywebsite1" value='@if(isset($website1)){!! $website1->host!!}@endif'/>

<input type="hidden" id="mywebsite2" value='@if(isset($website2)){!! $website2->host!!}@endif'/>

    <script>

        function compareWebsites(){
            var web1=$('#myformwebsite1').val();
            var web2=$('#myformwebsite2').val();
            location.replace("/compare/"+web1+"/"+web2);
        }


        $('select').selectpicker();
</script>
<script>
        $('#myformwebsite1').on('keyup',function() {

            $('#website1-list').html('');

            if($('#myformwebsite1').val()==null || $('#myformwebsite1').val().length==0){
                return;
            }

            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getweblist",
                data: {
                    'keyword': $('#myformwebsite1').val()
                },
                success: function (res) {
                    console.log(res);

                    $('#website1-list').html('');
                    res.forEach(function (value, key) {
                        console.log(value+key);
                        $('#website1-list').append('<li value="' + value + '">' + value + '</li>');
                    });

                },
                error: function (res) {
                    console.log(res);
                }
            });
        });

        $('#myformwebsite2').on('keyup',function(){

            $('#website2-list').html('');

            if($('#myformwebsite2').val()==null || $('#myformwebsite2').val().length==0){
                return;
            }
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/2/getweblist",
                data: {
                    'keyword': $('#myformwebsite2').val()
                },
                success: function (res) {
                    console.log(res);
                    $('#website2-list').html('');
                    res.forEach(function(value,key){
                        $('#website2-list').append('<li value="'+value+'">'+value+'</li>');
                    });

                },
                error:function(res){
                    console.log(res);
                }
            });

        });

        $(document).on('click','ul.results > li',function(){
           console.log($(this));
           console.log($(this).parent());
            console.log($(this).parent().parent());
            console.log($(this).parent().parent().find('input'));
            console.log($(this).attr('value'));


            $(this).parent().parent().find('input').val($(this).attr('value'));
            $(this).parent('ul').html('');
        });
    </script>


    <script>
        $('#website').on('keyup',function(){

            $('#website-list').html('');

            if($('#website').val()==null || $('#website').val().length==0){
                return;
            }
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/api/1/getweblist",
                data: {
                    'keyword': $('#website').val()
                },
                success: function (res) {
                    console.log(res);
                    $('#website-list').html('');
                    res.forEach(function(value,key){
                        $('#website-list').append('<li value="'+value+'">'+value+'</li>');
                    });

                },
                error:function(res){
                    console.log(res);
                }
            });

        });

        $(document).on('click','ul.results > li',function(){

            $(this).parent().parent().find('input').val($(this).attr('value'));
            $(this).parent('ul').html('');
        });
    </script>

</body>
</html>







