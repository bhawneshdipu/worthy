
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Worthy - Powerful dashboard</title>

    <!-- Latest Bootstrap compiled and minified CSS -->

    <!-- Latest Bootstrap compiled and minified CSS -->
    <link rel="stylesheet" href="/css/fontawesome.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/base.css">
    <link rel="stylesheet" href="/css/themes/cerulean.css">
    <link rel="stylesheet" href="/css/helper/themes/cerulean.css">
    <!--<link rel="stylesheet" href="http://gtools.bid/css/themes/cerulean.css">-->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>



<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://gtools.bid">Worthy</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://gtools.bid/top">Top</a></li>
                <li><a href="http://gtools.bid/recent">Recent</a></li>
            </ul>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Your Page Content Here -->

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Evaluate your site!</h1>
        <p>Insert an url to check.</p>
        <form id="report" method="post" name="report">
            @csrf
            <div class="input-group">
                <div class="input-group-addon">http://</div>
                <input type="text" name="website" id='website' class="form-control" placeholder="google.com" required>
                <span class="input-group-btn">
                <button class="btn btn-primary" type="submit">Go!</button>
              </span>
            </div><!-- /input-group -->
        </form>
        <div class="progress hide">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <span class="sr-only">45% Complete</span>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="row text-center">

        @if(isset($websites))
            @foreach($websites as $website)
                @if($website->hide==false)
                    @include('slot1')
                @else
                    @include('slot2')
                @endif
            @endforeach
        @endif
    </div>

    <div class="row text-center">
        {!! $websites->links() !!}
    </div>

    <hr>
</div>


<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><p>All rights reserved - 2018</p></div>
        </div>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="/js/bootstrap.min.js"></script>

<script src="/js/q.js"></script>

<script src="/js/report.js"></script>


</body>
</html>
