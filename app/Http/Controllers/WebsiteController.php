<?php

namespace App\Http\Controllers;

use App\DataSource\Alexa;
use App\DataSource\FacebookGraph;
use App\DataSource\GoogleAPI;
use App\DataSource\Pinterest;
use App\DataSource\Semrush;
use App\DataSource\Stumbleupon;
use App\DataSource\Validate;
use App\Graph;
use App\Setting;
use App\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Illuminate\View\Compilers\BladeCompiler;
use Response;
use Illuminate\Support\Facades\Log;
use JCrowe\BadWordFilter\BadWordFilter;

class WebsiteController extends Controller
{
    //
    protected  $validate;
    protected  $alexa;
    protected  $facebook;
    protected  $google;
    protected  $pinterest;
    protected  $semrush;
    protected  $stumbleupon;



    public function clean_url($url){
        $url=strtolower($url);
        $re = '/^(?:https?:\/\/)?(?:www\.)?/mi';
        $re = '/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n?]+)/im';
        $subst = '$1';
        preg_match_all($re, $url, $matches);
        //print_r($matches[0]);
        //$url= preg_replace($re, $subst, $url);
        if(isset($matches[1]) && isset($matches[1][0])){
            $url=$matches[1][0];
        }
        return $url;
    }
    public function index(){
        $websites = Website::paginate(8);

        return view('welcome')->with('websites', $websites);
    }
    public function valid(){
        Log::debug("valid();");
        if(request()->has('website')){
            $url=\request()->get('website');
            $url=$this->clean_url($url);
            Log::debug($url);
            $website=Website::where(['host'=>$url])->first();
            if($website!=null){
                return Response::json(array(
                    'success'=>true,
                    'msg'=>'Website already exist',
                    'redirect'=>array('host'=>$url)
                ),201);

            }
            $this->validate=new Validate($url);
            if($this->validate->url_exists()){

                $badwords=Setting::where('handle','bad_words')->first();
                # print_r($badwords->value);
                $bad_words=explode(',', $badwords->value);
                foreach ($bad_words as $key=>$value){
                    #print_r("@@@".$key."---".$value."---".$url."@@@");
                    #print_r("###".stripos($value,$url)."###");
                    if(stripos($url,$value) !== false) {
                        return Response::json(array(
                            'error'=>true,
                            'msg'=>'Invalid Website'
                        ),403);

                    }

                }

                $badwords_2=Setting::where('handle','bad_words_2')->first();

                $myOptions = array('strictness' => 'very_strict', 'also_check' => explode(',', $badwords_2->value));
                $filter = new BadWordFilter($myOptions);
                #print_r($badwords_2->value);
                #print_r($this->validate->getContent());
                #print_r($this->validate->getTitle());

                #print_r('####'.$filter->isDirty($this->validate->getContent())."####");

                if ($filter->isDirty(array($this->validate->getContent()))) {
                    Log::debug($filter->getDirtyWordsFromString($this->validate->getContent()));

                    Log::debug("Valid: Dirty Website;");
                    $object=Website::firstOrNew(['host'=>$url]);
                    $object->host=$this->clean_url(\request()->get('website'));;
                    $object->ip=$this->validate->getIp();
                    $object->title=$this->validate->getTitle();
                    $object->description=$this->validate->getDescription();
                    $object->keywords=$this->validate->getKeywords();
                    $object->response_time=$this->validate->getRespnoseTime();
                    $object->hide=true;
                    $mydate=new \DateTime();
                    $mydate=$mydate->format("Y-m-d");
                    $object->date= $mydate;

                    \request()->session()->put('object',$object);



                    Log::debug("Valid: Success;BadWords");
                    return Response::json(array(
                        'success'=>true,
                        'msg'=>'Valid URL with BadWords'
                    ),200);

                }else{

                    $object=Website::firstOrNew(['host'=>$url]);
                    $object->host=$this->clean_url(\request()->get('website'));;
                    $object->ip=$this->validate->getIp();
                    $object->title=$this->validate->getTitle();
                    $object->description=$this->validate->getDescription();
                    $object->keywords=$this->validate->getKeywords();
                    $object->response_time=$this->validate->getRespnoseTime();
                    $mydate=new \DateTime();
                    $mydate=$mydate->format("Y-m-d");
                    $object->date= $mydate;



                    \request()->session()->put('object',$object);

                    Log::debug("Valid: Success;");
                    return Response::json(array(
                        'success'=>true,
                        'msg'=>'Valid URL'
                    ),200);

                }
            }else{
                \request()->session()->forget('object');

                Log::debug("Valid: URL not found;");
                return Response::json(array(
                    'error' => true,
                    'msg' => 'Invalid Website'
                ), 403);

            }
        }else{
            \request()->session()->forget('object');

            Log::debug("Valid: Invalid Website;");
            return Response::json(array(
                'error' => true,
                'msg' => 'Invalid Website'
            ), 403);

        }
    }

    public function validOld(){
        Log::debug("validOld();");
        if(request()->has('website')){
            $url=\request()->get('website');
            $url=$this->clean_url($url);

            $this->validate=new Validate($url);
            if($this->validate->url_exists()){

                $badwords=Setting::where('handle','bad_words')->first();
                # print_r($badwords->value);
                $bad_words=explode(',', $badwords->value);
                foreach ($bad_words as $key=>$value){
                    #print_r("@@@".$key."---".$value."---".$url."@@@");
                    #print_r("###".stripos($value,$url)."###");
                    if(stripos($url,$value) !== false) {
                        return Response::json(array(
                            'error'=>true,
                            'msg'=>'Invalid Website'
                        ),403);

                    }

                }

                $badwords_2=Setting::where('handle','bad_words_2')->first();

                $myOptions = array('strictness' => 'very_strict', 'also_check' => explode(',', $badwords_2->value));
                $filter = new BadWordFilter($myOptions);
                #print_r($badwords_2->value);
                #print_r($this->validate->getContent());
                #print_r($this->validate->getTitle());

                #print_r('####'.$filter->isDirty($this->validate->getContent())."####");

                if ($filter->isDirty(array($this->validate->getContent()))) {
                    Log::debug($filter->getDirtyWordsFromString($this->validate->getContent()));

                    Log::debug("Valid: Dirty Website;");
                    $object=Website::firstOrNew(['host'=>$url]);
                    $object->host=$url;
                    $object->ip=$this->validate->getIp();
                    $object->title=$this->validate->getTitle();
                    $object->description=$this->validate->getDescription();
                    $object->keywords=$this->validate->getKeywords();
                    $object->response_time=$this->validate->getRespnoseTime();
                    $object->hide=true;
                    $mydate=new \DateTime();
                    $mydate=$mydate->format("Y-m-d");
                    $object->date= $mydate;

                    \request()->session()->put('object',$object);



                    Log::debug("Valid: Success;BadWords");
                    return Response::json(array(
                        'success'=>true,
                        'msg'=>'Valid URL with BadWords'
                    ),200);

                }else{

                    $object=Website::firstOrNew(['host'=>$url]);
                    $object->host=$url;
                    $object->ip=$this->validate->getIp();
                    $object->title=$this->validate->getTitle();
                    $object->description=$this->validate->getDescription();
                    $object->keywords=$this->validate->getKeywords();
                    $object->response_time=$this->validate->getRespnoseTime();
                    $mydate=new \DateTime();
                    $mydate=$mydate->format("Y-m-d");
                    $object->date= $mydate;



                    \request()->session()->put('object',$object);

                    Log::debug("Valid: Success;");
                    return Response::json(array(
                        'success'=>true,
                        'msg'=>'Valid URL'
                    ),200);

                }
            }else{
                \request()->session()->forget('object');

                Log::debug("Valid: URL not found;");
                return Response::json(array(
                    'error' => true,
                    'msg' => 'Invalid Website'
                ), 403);

            }
        }else{
            \request()->session()->forget('object');

            Log::debug("Valid: Invalid Website;");
            return Response::json(array(
                'error' => true,
                'msg' => 'Invalid Website'
            ), 403);

        }
    }

    public function google(){

        Log::debug("google();");
        if(request()->session()->has('object')){
            $this->google=new GoogleAPI(\request()->session()->get('object')->host);
            $object=\request()->session()->get('object');
            $object->screenshot=$this->google->getScreenshot();
            $object->desktop_speed_score=$this->google->getDesktopSpeedScore();
            $object->mobile_speed_score=$this->google->getMobileSpeedScore();
            $object->mobile_usablity_score=$this->google->getMobileUsablityScore();
            $object->desktop_page_stats=$this->google->getDesktopPageStats();
            $object->mobile_page_stats=$this->google->getMobilePageStats();

            \request()->session()->put('object',$object);

            Log::debug("google():success");
            return Response::json(array(
                'success'=>true,
                'msg'=>'Google '
            ),200);

        }else{

            \request()->session()->forget('object');

            Log::debug("google:error");
            return Response::json(array(
                'error' => true,
                'msg' => 'Error in Google Page Rank'
            ), 403);

        }
    }

    public function facebook(){

        Log::debug("facebook()");
        if(request()->session()->has('object')){
            $this->facebook=new FacebookGraph(\request()->session()->get('object')->host);
            $object=\request()->session()->get('object');
            $object->facebook_comment_count=$this->facebook->getCommentCount();
            $object->facebook_share_count=$this->facebook->getShareCount();
            \request()->session()->put('object',$object);

            Log::debug("facebook:success");
            return Response::json(array(
                'success'=>true,
                'msg'=>'Facebook'
            ),200);

        }else{
            \request()->session()->forget('object');

            Log::debug("facebook:error");
            return Response::json(array(
                'error' => true,
                'msg' => 'Error in Facebook Graph'
            ), 403);

        }
    }
    public function alexa(){

        Log::debug("alexa()");
        if(request()->session()->has('object')){
            $this->alexa=new Alexa(\request()->session()->get('object')->host);
            $object=\request()->session()->get('object');
            $object->alexa_rank=$this->alexa->getRank();
            $object->alexa_text=$this->alexa->getText();

            $object->alexa_delta=$this->alexa->getDelta();
            $object->alexa_country_rank=$this->alexa->getCountryRank();
            $object->alexa_country_code=$this->alexa->getCountryCode();
            $object->alexa_country_name=$this->alexa->getCountryName();

            $estimation=$this->alexa->processEstimation($object->alexa_rank);

            if(isset($estimation['daily_unique_visits'])){
                $object->daily_unique_visits=$estimation['daily_unique_visits'];
            }
            if(isset($estimation['daily_page_views'])) {

                $object->daily_page_views = $estimation['daily_page_views'];
            }
            if(isset($estimation['income_per_day'])) {
                $object->income_per_day = $estimation['income_per_day'];
            }
            if(isset($estimation['estimated_value'])) {
                $object->estimated_value = $estimation['estimated_value'];
            }
            \request()->session()->put('object',$object);

            Log::debug("alexa:success");
            return Response::json(array(
                'success'=>true,
                'msg'=>'Alexa'
            ),200);

        }else{
            \request()->session()->forget('object');

            Log::debug("alexa:error");
            return Response::json(array(
                'error' => true,
                'msg' => 'Error in Alexa'
            ), 403);

        }
    }
    public function semrush(){

        Log::debug("semrush()");
        if(request()->session()->has('object')){
            $this->semrush=new Semrush(\request()->session()->get('object')->host);
            $object=\request()->session()->get('object');
            $object->semrush_rank=$this->semrush->getRank();
            \request()->session()->put('object',$object);

            Log::debug("semrush:success");
            return Response::json(array(
                'success'=>true,
                'msg'=>'Semrush'
            ),200);

        }else{

            \request()->session()->forget('object');

            Log::debug("semrush:error");
            return Response::json(array(
                'error' => true,
                'msg' => 'Error in Semrush'
            ), 403);

        }
    }
    public function pinterest(){

        Log::debug("pinterest()");
        if(request()->session()->has('object')){
            $this->pinterest=new Pinterest(\request()->session()->get('object')->host);
            $object=\request()->session()->get('object');
            $object->pinterest=$this->pinterest->getCount();
            \request()->session()->put('object',$object);

            Log::debug("pinterest:success");
            return Response::json(array(
                'success'=>true,
                'msg'=>'Pinterest'
            ),200);

        }else{

            \request()->session()->forget('object');

            Log::debug("pinterest:error");
            return Response::json(array(
                'error' => true,
                'msg' => 'Error in Pinterest'
            ), 403);

        }
    }
    public function stumbleupon(){

        Log::debug("stumbleupon()");
        if(request()->session()->has('object')){
            $this->stumbleupon=new Stumbleupon(\request()->session()->get('object')->host);
            $object=\request()->session()->get('object');
            $object->stumbleupon=$this->stumbleupon->getViews();
            \request()->session()->put('object',$object);
            Log::debug("stumbleupon:success");

            return Response::json(array(
                'success'=>true,
                'msg'=>'Stumbleupon'
            ),200);

        }else{

            \request()->session()->forget('object');
            Log::debug("stumbleupon:error");

            return Response::json(array(
                'error' => true,
                'msg' => 'Error in Stumbleupon'
            ), 403);

        }
    }




    public function save(){
        Log::debug("save()");

        if(request()->session()->has('object')){
            $object=\request()->session()->get('object');

            $object->save();
            $this->saveInGraph($object);
            \request()->session()->forget('object');
            Log::debug("save:success");

            return Response::json(array(
                'success'=>true,
                'msg'=>'Website Saved',
                'url'=>$object->host
            ),200);
        }else{

            \request()->session()->forget('object');
            Log::debug("save:error");

            return Response::json(array(
                'error' => true,
                'msg' => 'Error in Saving'
            ), 403);

        }
    }



    public function store(){


        $url=request()->website;
        $url=strtolower($url);
        $validate=new Validate($url);
        print_r("validate");
        Log::debug("Validate");
        if($validate->url_exists()){
            $alexa=new Alexa($url);

            print_r("Alexa");

            Log::debug("Alexa");
            $facebook=new FacebookGraph($url);

            Log::debug("Facebook");
            print_r("Facebook");
            $google=new GoogleAPI($url);

            Log::debug("Google API");

            print_r("Google API");
            $pinterest=new Pinterest($url);

            Log::debug("Pinterest");

            print_r("Pinterest");
            $semrush=new Semrush($url);

            Log::debug("Semrush");
            print_r("Semrush");
            $stumbleupon=new Stumbleupon($url);

            print_r("Stumbleupon");

            Log::debug("Stumbleupon");
            $website=Website::firstOrNew(['host'=>$url]);
            $website->host=$url;
            $website->ip=$validate->getIp();
            $website->title=$validate->getTitle();
            $website->description=$validate->getDescription();
            $website->keywords=$validate->getKeywords();
            $website->response_time=$validate->getRespnoseTime();
            //alexa
            $website->alexa_rank=$alexa->getRank();
            $website->alexa_text=$alexa->getText();

            $website->alexa_delta=$alexa->getDelta();
            $website->alexa_country_rank=$alexa->getCountryRank();
            $website->alexa_country_code=$alexa->getCountryCode();
            $website->alexa_country_name=$alexa->getCountryName();

            $website->semrush_rank=$semrush->getRank();

            $website->facebook_comment_count=$facebook->getCommentCount();
            $website->facebook_share_count=$facebook->getShareCount();
            $website->stumbleupon=$stumbleupon->getViews();
            $website->pinterest=$pinterest->getCount();

            $website->screenshot=$google->getScreenshot();
            $website->desktop_speed_score=$google->getDesktopSpeedScore();
            $website->mobile_speed_score=$google->getMobileSpeedScore();
            $website->mobile_usablity_score=$google->getMobileUsablityScore();
            $website->desktop_page_stats=$google->getDesktopPageStats();
            $website->mobile_page_stats=$google->getMobilePageStats();
            $mydate=new \DateTime();
            $mydate=$mydate->format("Y-m-d");
            $website->date= $mydate;

            $badwords=Setting::where('handle','bad_words')->first();
            $myOptions = array('strictness' => 'very_strict', 'also_check' => explode(',', $badwords->value));

            $filter = new BadWordFilter($myOptions);

            if ($filter->isDirty(array($validate->getContent()))) {
                print_r('#########################');
                print_r($filter->getDirtyWordsFromString($validate->getContent()));
                $website->hide = true;
            }
            $website->save();
            $this->saveInGraph($website);
            return "success";
        }else{
            return 'Hello erorr ';
        }

    }


    public function saveInGraph($website){
        Log::debug("saveInGraph()");

        $date=new\DateTime();
        $graph=Graph::firstOrNew(['host'=>$website->host,'date'=>$date->format('Y-m-d')]);
        if($website->host!=null){
            $graph->host=$website->host;
        }
        if($website->ip!=null){
            $graph->ip=$website->ip;
        }
        if($website->title!=null){
            $graph->title=$website->title;
        }
        if($website->description!=null){
            $graph->description=$website->description;
        }
        if($website->keywords!=null){
            $graph->keywords=$website->keywords;
        }
        if($website->response_time!=null){
            $graph->response_time=$website->response_time;
        }
        if($website->screenshot!=null){
            $graph->screenshot=$website->screenshot;
        }

        //Alexa
        if($website->alexa_rank!=null){
            $graph->alexa_rank=$website->alexa_rank;
        }

        if($website->alexa_delta!=null){
            $graph->alexa_delta=$website->alexa_delta;
        }
        if($website->alexa_text!=null){
            $graph->alexa_text=$website->alexa_text;
        }

        //Alexacountryrank
        if($website->alexa_country_code!=null){
            $graph->alexa_country_code=$website->alexa_country_code;
        }
        if($website->alexa_country_rank!=null){
            $graph->alexa_country_rank=$website->alexa_country_rank;
        }
        if($website->alexa_country_name!=null){
            $graph->alexa_country_name=$website->alexa_country_name;
        }

        if($website->daily_unique_visits!=null){
            $graph->daily_unique_visits=$website->daily_unique_visits;
        }
        if($website->daily_page_views!=null) {

            $graph->daily_page_views = $website->daily_page_views;
        }
        if($website->income_per_day!=null) {
            $graph->income_per_day = $website->income_per_day;
        }
        if($website->estimated_value!=null) {
            $graph->estimated_value = $website->daily_unique_visits;
        }

        //Semrushgraph
        if($website->semrush_rank!=null) {
            $semrush_data = $website->semrush_rank;
            $graph->semrush_rank = $website->semrush_rank;

            $semrush_data = json_decode($website->semrush_rank, true);

            if ($semrush_data['Rk'] != null) {
                $graph->semrush_Rk = $semrush_data['Rk'];
            }
            if ($semrush_data['Or'] != null) {
                $graph->semrush_Or = $semrush_data['Or'];
            };
            if ($semrush_data['Ot'] != null) {
                $graph->semrush_Ot = $semrush_data['Ot'];
            };
            if ($semrush_data['Oc'] != null) {
                $graph->semrush_Oc = $semrush_data['Oc'];
            };
        }
        //Socialshares
        if($website->facebook_comment_count!=null){
            $graph->facebook_comment_count=$website->facebook_comment_count;
        }
        if($website->facebook_share_count!=null){
            $graph->facebook_share_count=$website->facebook_share_count;
        }
        if($website->stumbleupon!=null){
            $graph->stumbleupon=$website->stumbleupon;
        }
        if($website->pinterest!=null){
            $graph->pinterest=$website->pinterest;
        }

        //GoogleApi
        if($website->desktop_speed_score!=null){
            $graph->desktop_speed_score=$website->desktop_speed_score;
        }
        if($website->mobile_speed_score!=null){
            $graph->mobile_speed_score=$website->mobile_speed_score;
        }
        if($website->mobile_usablity_score!=null){
            $graph->mobile_usablity_score=$website->mobile_usablity_score;
        }
        if($website->mobile_page_stats!=null){
            $graph->mobile_page_stats=$website->mobile_page_stats;
        }
        if($website->desktop_page_stats!=null){
            $graph->desktop_page_stats=$website->desktop_page_stats;
        }
        if($website->hide!=null){
            $graph->hide=$website->hide;
        }
        $date=new\DateTime();
        $graph->date=$date->format('Y-m-d');


        $graph->save();
    }


}
