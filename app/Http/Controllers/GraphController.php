<?php

namespace App\Http\Controllers;

use App\Graph;
use App\Setting;
use App\Website;
use Illuminate\Http\Request;
use JCrowe\BadWordFilter\BadWordFilter;

class GraphController extends Controller
{
    //


    public function clean_url($url){
        $url=strtolower($url);
        $re = '/^(?:https?:\/\/)?(?:www\.)?/mi';
        $re = '/^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n?]+)/im';
        $subst = '$1';
        preg_match_all($re, $url, $matches);
        //print_r($matches[0]);
        //$url= preg_replace($re, $subst, $url);
        if(isset($matches[1]) && isset($matches[1][0])){
            $url=$matches[1][0];
        }
        return $url;
    }

    public function index($url){
        $url=$this->clean_url($url);
        $website=Website::where('host',$url)->first();


        $badwords_2=Setting::where('handle','bad_words_2')->first();

        $myOptions = array('strictness' => 'very_strict', 'also_check' => explode(',', $badwords_2->value));
        $filter = new BadWordFilter($myOptions);
        $str=$website->title.",".$website->description.", ".$website->keywords;
        if ($filter->isDirty(array($str))) {
            $website->hide=true;
            $website->save();
        }

        $website->showhost=$filter->clean($website->host);
        $recent=Website::take(10)->get();


        foreach($recent as $web){

            $badwords_2=Setting::where('handle','bad_words_2')->first();

            $myOptions = array('strictness' => 'very_strict', 'also_check' => explode(',', $badwords_2->value));
            $filter = new BadWordFilter($myOptions);
            $web->showhost=$filter->clean($web->host);
            $bad_words=explode(',', $badwords_2->value);

            foreach ($bad_words as $key=>$value){
                if(stripos($web->host,$value) !== false) {
                    $web->showhost= str_ireplace($value,str_repeat("*",strlen($value)),$web->showhost);

                }
            }
        }

        $similar=Website::take(10)->get();

        foreach($similar as $web){

            $badwords_2=Setting::where('handle','bad_words_2')->first();

            $myOptions = array('strictness' => 'very_strict', 'also_check' => explode(',', $badwords_2->value));
            $filter = new BadWordFilter($myOptions);
            $web->showhost=$filter->clean($web->host);
            $bad_words=explode(',', $badwords_2->value);
            foreach ($bad_words as $key=>$value){
                if(stripos($web->host,$value) !== false) {
                    $web->showhost= str_ireplace($value,str_repeat("*",strlen($value)),$web->showhost);

                }
            }
        }

        $slot1=Setting::where('handle','slot_1')->first();
        $slot2=Setting::where('handle','slot_2')->first();

        return view('graph',compact(['website','recent','similar','slot1','slot2']));}

    public function getAlexaXData(){
        $days=request('days',30);
        $website=request('website');
        //echo $website;

        $website=$this->clean_url($website);
        $rows=Graph::where('host','like',$website.'%')->select('date')->orderBy('date','desc')->limit($days)->get();

        $data=array();
        foreach ($rows as $row){
            $i=new \DateTime($row['date']);
            $data[]=$i->format('Y-m-d');;
        }
        $data=array_reverse($data);
        return json_encode($data);

    }
    public function getAlexaYData(){
        //echo $website;

        $days=request('days',30);
        $website=request('website');

        $website=$this->clean_url($website);
        $data_rank=array();
        $data_text=array();
        $data_delta=array();

        $data_country_rank=array();
        $data_country_name=array();
        $data_country_code=array();

        $data_daily_unique_visits=array();
        $data_daily_page_views=array();
        $data_income_per_day=array();
        $data_estimated_value=array();

        $rows=Graph::where('host','like',$website.'%')->orderBy('date','desc')->limit($days)->get();
        foreach ($rows as $row){
            $data_rank[]=$row['alexa_rank'];
            $data_text[]=$row['alexa_text'];
            $data_delta[]=$row['alexa_delta'];

            $data_country_rank[]=$row['alexa_country_rank'];
            $data_country_code[]=$row['alexa_country_code'];
            $data_country_name[]=$row['alexa_country_name'];


            $data_daily_unique_visits[]=$row['daily_unique_visits'];
            $data_daily_page_views[]=$row['daily_page_views'];
            $data_income_per_day[]=$row['income_per_day'];
            $data_estimated_value[]=$row['estimated_value'];


        }

        $data_rank=array_reverse($data_rank);
        $data_text=array_reverse($data_text);
        $data_delta=array_reverse($data_delta);

        $data_country_code=array_reverse($data_country_code);
        $data_country_name=array_reverse($data_country_name);
        $data_country_rank=array_reverse($data_country_rank);


        $data_daily_unique_visits=array_reverse($data_daily_unique_visits);
        $data_daily_page_views=array_reverse($data_daily_page_views);
        $data_income_per_day=array_reverse($data_income_per_day);
        $data_estimated_value=array_reverse($data_estimated_value);



        $data=array();
        $data['rank']=$data_rank;
        $data['text']=$data_text;
        $data['delta']=$data_text;

        $data['country_rank']=$data_country_rank;
        $data['country_name']=$data_country_name;
        $data['country_code']=$data_country_code;

        $data['daily_unique_visits']=$data_daily_unique_visits;
        $data['daily_page_views']=$data_daily_page_views;
        $data['income_per_day']=$data_income_per_day;
        $data['estimated_value']=$data_estimated_value;
        return json_encode($data);
    }



    public function getSumrushXData(){

        $days=request('days',30);
        $website=request('website');
        //echo $website;

        $website=$this->clean_url($website);

        $rows=Graph::where('host','like',$website.'%')->select('date')->orderBy('date','desc')->limit($days)->get();

        $data=array();
        foreach ($rows as $row){
            $i=new \DateTime($row['date']);
            $data[]=$i->format('Y-m-d');;
        }
        $data=array_reverse($data);
        return json_encode($data);

    }
    public function getSumrushYData(){

        $days=request('days',30);
        $website=request('website');
        //echo $website;

        $website=$this->clean_url($website);

        $data_rk=array();
        $data_or=array();
        $data_ot=array();
        $data_oc=array();
        $rows=Graph::where('host','like',$website.'%')->orderBy('date','desc')->limit($days)->get();
        foreach ($rows as $row){
            $data=json_decode($row['semrush_rank'],true);
            $data=json_encode($data);

            $data=json_decode($data,true);
            $data_rk[]=$data['Rk'];
            $data_or[]=$data['Or'];
            $data_ot[]=$data['Ot'];
            $data_oc[]=$data['Oc'];

        }

        $data_rk=array_reverse($data_rk);
        $data_or=array_reverse($data_or);
        $data_ot=array_reverse($data_ot);
        $data_oc=array_reverse($data_oc);

        $data=array();
        $data['Rk']=$data_rk;
        $data['Or']=$data_or;
        $data['Ot']=$data_ot;
        $data['Oc']=$data_oc;


        return json_encode($data);
    }


    public function getXData(){
        $days=request('days',30);
        $website=request('website');
        //echo $website;

        $website=$this->clean_url($website);
        $rows=Graph::where('host','like',$website.'%')->select('date')->orderBy('date','desc')->limit($days)->get();

        $data=array();
        foreach ($rows as $row){
            $i=new \DateTime($row['date']);
            $data[]=$i->format('Y-m-d');;
        }
        $data=array_reverse($data);
        return json_encode($data);

    }
    public function getYResponseTime(){
        $days=request('days',30);
        $website=request('website');
        //echo $website;


        $website=$this->clean_url($website);

        $rows=Graph::where('host','like',$website.'%')->orderBy('date','desc')->limit($days)->get();

        $data=array();
        foreach ($rows as $row){
            $data[]=$row->response_time;;
        }
        $data=array_reverse($data);
        return json_encode($data);

    }

    public function getYFacebookCount(){
        $days=request('days',30);
        $website=request('website');

        $website=$this->clean_url($website);
        $rows=Graph::where('host','like',$website.'%')->select('date')->orderBy('date','desc')->limit($days)->get();

        $data_share=array();
        $data_comment=array();

        $rows=Graph::where('host','like',$website.'%')->orderBy('date','desc')->limit($days)->get();
        foreach ($rows as $row){

            $data_comment[]=$row['facebook_comment_count'];
            $data_share[]=$row['facebook_share_count'];

        }

        $data_comment=array_reverse($data_comment);
        $data_share=array_reverse($data_share);

        $data=array();
        $data['comment']=$data_comment;
        $data['share']=$data_share;

        return json_encode($data);
    }
    public function getYSpeedScore(){
        $days=request('days',30);
        $website=request('website');

        $website=$this->clean_url($website);

        $rows=Graph::where('host','like',$website.'%')->orderBy('date','desc')->limit($days)->get();

        $data_desktop=array();
        $data_mobile=array();
        $rows=Graph::where('host','like',$website.'%')->orderBy('date','desc')->limit($days)->get();
        foreach ($rows as $row){

            $data_desktop[]=$row['desktop_speed_score'];
            $data_mobile[]=$row['mobile_speed_score'];

        }

        $data_desktop=array_reverse($data_desktop);
        $data_mobile=array_reverse($data_mobile);

        $data=array();
        $data['desktop']=$data_desktop;
        $data['mobile']=$data_mobile;

        return json_encode($data);
    }


    public function getYMobileUsabilityScore(){
        $days=request('days',30);
        $website=request('website');
        //echo $website;

        $website=$this->clean_url($website);
        $rows=Graph::where('host','like',$website.'%')->orderBy('date','desc')->limit($days)->get();

        $data=array();
        foreach ($rows as $row){

            $data[]=$row->mobile_usablity_score;;
        }
        $data=array_reverse($data);
        return json_encode($data);

    }


    public function getYStumbleupon(){
        $days=request('days',30);
        $website=request('website');
        //echo $website;

        $website=$this->clean_url($website);

        $rows=Graph::where('host','like',$website.'%')->orderBy('date','desc')->limit($days)->get();

        $data=array();
        foreach ($rows as $row){
            $data[]=$row->stumbleupon;;
        }
        $data=array_reverse($data);
        return json_encode($data);

    }


    public function getYPinterest(){
        $days=request('days',30);
        $website=request('website');
        //echo $website;


        $website=$this->clean_url($website);
        $rows=Graph::where('host','like',$website.'%')->orderBy('date','desc')->limit($days)->get();

        $data=array();
        foreach ($rows as $row){
            $data[]=$row->pinterest;;
        }
        $data=array_reverse($data);
        return json_encode($data);

    }

    public function getPageStats(){
        $days=request('days',30);
        $website=request('website');

        $website=$this->clean_url($website);

        $data_desktop=array();
        $data_mobile=array();
        $row=Website::where('host','like',$website.'%')->first();
        $data_desktop[]=$row['desktop_page_stats'];
        $data_mobile[]=$row['mobile_page_stats'];

        $data=array();
        $data['desktop']=$data_desktop;
        $data['mobile']=$data_mobile;

        return json_encode($data);
    }


}
