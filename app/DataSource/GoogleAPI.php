<?php

namespace App\DataSource;


use Illuminate\Support\Facades\File;

class GoogleAPI
{

    protected  $domain;
    protected  $json_mobile;
    protected $json_desktop;
    public function __construct($domain)
    {

        if(strpos($domain,'http')==false){
            $domain='http://'.$domain;
        }
        $this->domain=$domain;
        $url='https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url='.$domain.'&screenshot=true&strategy=desktop&key=AIzaSyCHjQwBJ8B0Y40NQAug_g9iWCzmILqnQ-o';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

        $data = curl_exec($ch); // execute curl request
        curl_close($ch);
        $this->json_desktop=json_decode($data,true);


        $url='https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url='.$domain.'&strategy=mobile&key=AIzaSyCHjQwBJ8B0Y40NQAug_g9iWCzmILqnQ-o';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

        $data = curl_exec($ch); // execute curl request
        curl_close($ch);
        $this->json_mobile=json_decode($data,true);

    }
    public function getDesktopSpeedScore(){

        if(isset($this->json_desktop['ruleGroups']['SPEED']['score'])){
            return $this->json_desktop['ruleGroups']['SPEED']['score'];
        }else{
            return "";
        }

    }


    public function getMobileSpeedScore(){


        if(isset($this->json_mobile['ruleGroups']['SPEED']['score'])){
            return $this->json_mobile['ruleGroups']['SPEED']['score'];
        }else{
            return "";
        }


    }
    public function  getMobileUsablityScore(){

        if(isset($this->json_mobile['ruleGroups']['USABILITY']['score'])){
            return $this->json_mobile['ruleGroups']['USABILITY']['score'];
        }else{
            return "";
        }
    }
    public function getDesktopPageStats(){

        if(isset($this->json_desktop['pageStats'])){
            return json_encode($this->json_desktop['pageStats']);
        }else{
            return "";
        }

    }

    public function getMobilePageStats(){


        if(isset($this->json_mobile['pageStats'])){
            return json_encode($this->json_mobile['pageStats']);
        }else{
            return "";
        }
    }
    public function getScreenshot(){
        //screenshot data
        $filePath='';
        if($this->json_desktop['screenshot']){
            $this->saveScreenshot($this->json_desktop['screenshot']['data'],$this->domain);
        }else{
            $filePath = $this->getDestpath($this->domain);
            File::copy(public_path(). '/images/no-preview.jpg', $filePath);

        }
        return $this->getReferencePath($this->domain);
    }
    private function saveScreenshot($base64,$site)
    {
        if($base64){

            $filePath = $this->getDestpath($site);

            $fix_base64 = str_replace(["_",'-'], ['/','+'],$base64);

            $image = base64_decode($fix_base64);

            file_put_contents($filePath, $image);
            return $filePath;
        }
    }

    private function getHost($url)
    {
        if(filter_var($url, FILTER_VALIDATE_URL)){
            $website = parse_url($url);
            return $website['host'];
        }
        return $url;
    }
    private function getDestPath($site)
    {
        $website = $this->getHost($site);

        $destinationPath = public_path() . '/sites/' . str_replace('.','_',$website);
        $filePath = $destinationPath.'/'.str_replace('.','_',$website).'.jpg';
        if(!File::exists($destinationPath)) {
            // path does not exist, let's create it
            File::makeDirectory($destinationPath, $mode = 0777, true, true);
        }

        return $filePath;
    }
    public  function getReferencePath($website){
        $website=$this->getHost($website);
        return  '/sites/' .str_replace('.','_',$website).'/'. str_replace('.','_',$website).'.jpg';
    }





}