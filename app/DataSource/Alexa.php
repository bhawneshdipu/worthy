<?php

namespace App\DataSource;


class Alexa
{
    protected  $uri='http://data.alexa.com/data?cli=10&dat=snbamz&url=';

    protected $xml;
/*    protected $alexa_rank;

    protected $alexa_popularity;
    protected $alexa_delta;
    protected $alexa_country_code;

    protected $alexa_country_rank;
    protected $alexa_country_name;*/


    public function __construct($domain)
    {
        $url=$this->uri.$domain;
        $this->xml = simplexml_load_file($url);
        //print_r($this->xml);
    }

    public function getText(){
        if(isset($this->xml->SD[1]->POPULARITY)){
            $data = (int) $this->xml->SD[1]->POPULARITY->attributes()->TEXT;

            return $data;
        }else{
            if(isset($this->xml->SD[0]->POPULARITY)){
                $data = (int) $this->xml->SD[0]->POPULARITY->attributes()->TEXT;
    
                return $data;
            }else{
                return 'N/A';
            }
            return 'N/A';
        }
    }
    public function getRank(){
        if(isset($this->xml->SD[1]->REACH)){
            $data = (int) $this->xml->SD[1]->REACH->attributes()->RANK;
            return $data;
        }else{
                if(isset($this->xml->SD[0]->REACH)){
                    $data = (int) $this->xml->SD[0]->REACH->attributes()->RANK;
                    return $data;
                }else{
                    return 'N/A';
                }

            return 'N/A';
        }
    }
    public function getDelta(){
        if(isset($this->xml->SD[1]->RANK)){
            $data = (int) $this->xml->SD[1]->RANK->attributes()->DELTA;
            return $data;
        }else{
                if(isset($this->xml->SD[0]->RANK)){
                    $data = (int) $this->xml->SD[0]->RANK->attributes()->DELTA;
                    return $data;
                }else{
                    return 'N/A';
                }

            return 'N/A';
        }
    }
    public function getCountryCode(){
        if(isset($this->xml->SD[1]->COUNTRY)){
            $data =  (string)$this->xml->SD[1]->COUNTRY->attributes()->CODE;
            return $data;
        }else{
                if(isset($this->xml->SD[0]->COUNTRY)){
                    $data =  (string)$this->xml->SD[0]->COUNTRY->attributes()->CODE;
                    return $data;
                }else{
                    return 'N/A';
                }
            
            return 'N/A';
        }
    }
    public function getCountryName(){
        if(isset($this->xml->SD[1]->COUNTRY)){
            $data = (string) $this->xml->SD[1]->COUNTRY->attributes()->NAME;
            return $data;
        }else{
                    if(isset($this->xml->SD[0]->COUNTRY)){
                        $data = (string) $this->xml->SD[0]->COUNTRY->attributes()->NAME;
                        return $data;
                    }else{
                        return 'N/A';
                    }

            return 'N/A';
        }
    }
    public function getCountryRank(){
        if(isset($this->xml->SD[1]->COUNTRY)){
            $data = (int) $this->xml->SD[1]->COUNTRY->attributes()->RANK;
            return $data;
        }else{
                    if(isset($this->xml->SD[0]->COUNTRY)){
                        $data = (int) $this->xml->SD[0]->COUNTRY->attributes()->RANK;
                        return $data;
                    }else{
                        return 'N/A';
                    }

            return 'N/A';
        }
    }

    /**
     * Get the site value
     *
     * @param $alexa_rank
     */
    public static function processEstimation($alexa_rank)
    {
        if(is_numeric ( $alexa_rank )==false){
            $daily_unique_visitors = "0";
            $daily_pageviews       = "0";
            $income_per_day        = "0";
            $domain_worth          = "0";
            $response = array(
                'daily_unique_visits' => $daily_unique_visitors,
                'daily_page_views' => $daily_pageviews,
                'income_per_day' => ($income_per_day > 0 ? $income_per_day : 0.15),
                'estimated_value' => ($domain_worth > 0 ? $domain_worth : 8.95)
            );
            return $response;

        }
        $total_internet_users = 1009000000;
        if (empty($alexa_rank)) {
            $daily_unique_visitors = "0";
            $daily_pageviews       = "0";
            $income_per_day        = "0";
            $domain_worth          = "0";
        } else {
            if ($alexa_rank <= 10000) {
                $average_pageviews        = "8";
                $average_reach_percentage = "42.5";
                $value_per_pageview       = "0.1";
                $value_month              = "36";
            } elseif ($alexa_rank <= 100000) {
                $average_pageviews        = "6";
                $average_reach_percentage = "40";
                $value_per_pageview       = "0.2";
                $value_month              = "24";
            } elseif ($alexa_rank <= 200000) {
                $average_pageviews        = "5";
                $average_reach_percentage = "38";
                $value_per_pageview       = "0.25";
                $value_month              = "20";
            } elseif ($alexa_rank <= 400000) {
                $average_pageviews        = "4";
                $average_reach_percentage = "35";
                $value_per_pageview       = "0.28";
                $value_month              = "18";
            } elseif ($alexa_rank <= 500000) {
                $average_pageviews        = "3";
                $average_reach_percentage = "30";
                $value_per_pageview       = "0.30";
                $value_month              = "12";
            } else {
                $average_pageviews        = "2";
                $average_reach_percentage = "20";
                $value_per_pageview       = "0.31";
                $value_month              = "8";
            }
            $daily_unique_visitors = round((($total_internet_users * $average_reach_percentage) / 100) / $alexa_rank);
            $daily_pageviews       = round($daily_unique_visitors * $average_pageviews);
            $income_per_day        = round(($value_per_pageview * $daily_pageviews) / 100);
            $domain_worth          = round(($income_per_day * 30 * $value_month));
        }
        $response = array(
            'daily_unique_visits' => $daily_unique_visitors,
            'daily_page_views' => $daily_pageviews,
            'income_per_day' => ($income_per_day > 0 ? $income_per_day : 0.15),
            'estimated_value' => ($domain_worth > 0 ? $domain_worth : 8.95)
        );
        return $response;
    }







}