<?php
/**
 * Created by PhpStorm.
 * User: dipu
 * Date: 21/6/18
 * Time: 4:00 PM
 */

namespace App\DataSource;


class VK
{
    protected  $url='https://vk.com/share.php?act=count&index=1&url=';
    protected  $xml;
    public function __construct($domain)
    {

        if(strpos($domain,'http')==false){
            $domain='http://'.$domain;
        }

        $url=$this->url.$domain;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

        $data = curl_exec($ch); // execute curl request
        curl_close($ch);
        $this->xml=$data;


        $re = '/[(](.*)[)]/m';
        $str = $this->xml;


        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
        $this->xml=$matches[0];


    }

    public function getShareCount(){
        return explode(',', $this->xml)[1];
    }



}