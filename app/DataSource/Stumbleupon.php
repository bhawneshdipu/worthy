<?php
/**
 * Created by PhpStorm.
 * User: dipu
 * Date: 21/6/18
 * Time: 3:52 PM
 */

namespace App\DataSource;


class Stumbleupon
{

    protected  $url='http://www.stumbleupon.com/services/1.01/badge.getinfo?url=';
    protected  $json;
    public function __construct($domain)
    {

        if(strpos($domain,'http')==false){
            $domain='http://'.$domain;
        }

        $url=$this->url.$domain;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

        $data = curl_exec($ch); // execute curl request
        curl_close($ch);
        $this->json=json_decode($data,true);

    }
    public function getViews(){
        if(isset($this->json['result'])){
            if(isset($this->json['result']['views'])){
                $views=$this->json['result']['views'];

            }else{
                $views=0;
            }

        }else{
            $views=0;
        }
    return $views;

    }
    public function getTitle(){

        if(isset($this->json['result'])){
            if(isset($this->json['result']['title'])){
                $title=$this->json['result']['title'];

            }else{
                $title=0;
            }

        }else{
            $title=0;
        }

        return $title;
    }

}