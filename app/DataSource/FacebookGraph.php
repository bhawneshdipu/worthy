<?php
namespace App\DataSource;


class FacebookGraph
{
    protected  $url='https://graph.facebook.com/?id=';
    protected  $json;
    public function __construct($domain)
    {

        if(strpos($domain,'http')==false){
            $domain='http://'.$domain;
        }
        $url=$this->url.$domain;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

        $data = curl_exec($ch); // execute curl request
        curl_close($ch);
        $this->json=json_decode($data,true);

    }
    public function getShareCount(){

        if(isset($this->json['share'])){
            if(isset($this->json['share']['share_count'])){
                $share_count=$this->json['share']['share_count'];

            }else{
                $share_count=0;
            }


        }else{
            $share_count=0;
        }

        return $share_count;
    }

    public function getCommentCount(){

        if(isset($this->json['share'])){
            if(isset($this->json['share']['comment_count'])){
                $comment_count=$this->json['share']['comment_count'];
            }else{
                $comment_count=0;
            }

        }else{
            $comment_count=0;
        }

        return $comment_count;
    }

    public function getDescription(){
        if(isset($this->json['og_object'])){
            if(isset($this->json['og_object']['description'])){
                $description=$this->json['og_object']['description'];

            }else{
                $description='';
            }


        }else{
            $description='';
        }

        return $description;
    }

    public function getTitle(){
        if(isset($this->json['og_object'])){
            if(isset($this->json['og_object']['title'])){
                $title=$this->json['og_object']['title'];

            }else{
                $title='';
            }
        }else{
            $title='';
        }

        return $title;
    }


}