<?php

namespace App\DataSource;


class Pinterest
{


    public $url='https://api.pinterest.com/v1/urls/count.json?url=';

    public $json;
    public function __construct($domain)
    {


        if(strpos($domain,'http')==false){
            $domain='http://'.$domain;
        }
        $url=$this->url.$domain;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

        $data = curl_exec($ch); // execute curl request
        curl_close($ch);
        $this->json=$data;

        $re = '/[(](.*)[)]/m';
        $str = $this->json;


        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
        $this->json=json_decode($matches[0][1],true);

    }

    public function getCount(){
        if(isset($this->json['count'])){
            $count=$this->json['count'];
        }else{
            $count=0;
            $count=0;
        }

        return $count;
    }


}