<?php

namespace App\DataSource;


use Symfony\Component\DomCrawler\Crawler;

class Validate
{

    public $url;
    public $domain;
    public $data;
    public $title;
    public $description;
    public $keywords;
    public $content;
    public $request;
    public  $crawler;
    public  function __construct($url)
    {
        $this->httpclient = new \GuzzleHttp\Client();
        $url = trim($url);
        $url = str_ireplace("\n", "",$url);
        $url = str_ireplace("\r", "",$url);

        $this->domain=$url;
        if(strpos($url,'http')==false){
            $url='http://'.$url;
        }
        $this->url=$url;
        $this->request= $this->httpclient->request('GET',$this->url );
        $this->crawler=new Crawler($this->request->getBody()->getContents());

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $data = curl_exec($ch);
        curl_close($ch);

        $this->data=$data;

        $html = $this->data;

       // print_r($html);
        //parsing begins here:
        $doc = new \DOMDocument();
        @$doc->loadHTML($html);

        //get and display what you need:
        try{
        	$nodes = $doc->getElementsByTagName('title');
			$title = $nodes->item(0)->nodeValue;

		}catch(\Exception $e){

    		        
		}
		try{
			$nodes = $doc->getElementsByTagName('Title');
			$title = $nodes->item(0)->nodeValue;
			
		}catch(\Exception $e){

		}

		try{


	
        $metas = $doc->getElementsByTagName('meta');

        $description='';
        $keywords='';
        for ($i = 0; $i < $metas->length; $i++)
        {
            $meta = $metas->item($i);
            if($meta->getAttribute('name') == 'description')
                $description = $meta->getAttribute('content');

            if($meta->getAttribute('name') == 'Description')
                $description = $meta->getAttribute('content');

            if($meta->getAttribute('name') == 'keywords')
                $keywords = $meta->getAttribute('content');


            if($meta->getAttribute('name') == 'Keywords')
                $keywords = $meta->getAttribute('content');

            $this->content.=', '.$meta->getAttribute('content');
        }



        $this->title=$title;
        $this->description=$description;
        $this->keywords=$keywords;


        }catch (\Exception $e){
            // print_r($e);
        }
    }

    public function getIp(){
        return gethostbyname($this->domain);
    }
    public function getRespnoseTime()
    {
        $Start = microtime(true);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0');
        curl_setopt($ch, CURLOPT_REFERER, 'http://google.com');
        $html = curl_exec($ch);
        curl_close($ch);
        $End = microtime(true);
        $Time = $End - $Start;

        return $Time;


    }
    public function url_exists() {

        if($this->request->getStatusCode()>=200 and $this->getStatusCode()<400){
            return true;
        }
        return false;
/*        if (!$fp = curl_init($this->url))
            return false;
        return true;*/
    }

    public  function getTitle(){
        if(strlen($this->title)==0){
            try{
                if($this->title=$this->crawler->filter('title')->count()>0){
                    $this->title=$this->crawler->filter('title')->text();
                    return $this->title;

                }

            }catch(\Exception $e){
                return null;
            }
        }else{

            return $this->title;
        }
    }
    public function getDescription(){
        if(strlen($this->description)==0){
            try{
                if($this->crawler->filterXpath('//meta[@name="description"]')->count()>0){

                    return $this->crawler->filterXpath('//meta[@name="description"]')->attr('content');
                }
                if($this->crawler->filterXpath('//meta[@name="Description"]')->count()>0){

                    return $this->crawler->filterXpath('//meta[@name="Description"]')->attr('content');
                }

            }catch(\Exception $e){
                return null;
            }
        }else{

            return $this->description;

        }
    }
    public function getKeywords(){

        if(strlen($this->keywords)==0){
            try{
                if($this->crawler->filterXpath('//meta[@name="keywords"]')->count()>0){
                    return $this->crawler->filterXpath('//meta[@name="keywords"]')->attr('content');

                }

                if($this->crawler->filterXpath('//meta[@name="Keywords"]')->count()>0){
                    return $this->crawler->filterXpath('//meta[@name="Keywords"]')->attr('content');

                }

            }catch(\Exception $e){
                return null;
            }
        }else{

            return $this->keywords;

        }
    }

    public function getContent(){
        $this->content=$this->content.', '.$this->getTitle().",".$this->getDescription();

        return $this->content;
    }

    public function getStatusCode(){
        return $this->request->getStatusCode();

    }
    public function getBody(){
        return $this->request->getBody();

    }
}