<?php
/**
 * Created by PhpStorm.
 * User: dipu
 * Date: 20/6/18
 * Time: 6:15 PM
 */

namespace App\DataSource;


use Illuminate\Support\Facades\Log;

class Semrush
{

    public $semrush='https://us.backend.semrush.com/?action=report&type=domain_rank&domain=';

    protected $json;
    /*    protected $alexa_rank;

        protected $alexa_popularity;
        protected $alexa_delta;
        protected $alexa_country_code;

        protected $alexa_country_rank;
        protected $alexa_country_name;*/


    public function __construct($domain)
    {

        if(strpos($domain,'http')==false){
            $domain='http://'.$domain;
        }

        $url=$this->semrush.$domain;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

        $data = curl_exec($ch); // execute curl request
        curl_close($ch);
        $this->json=json_decode($data,true);

    }

    public function getRank(){
        if(isset($this->json['rank'])){
            $rank=$this->json['rank'];
            if(isset($rank['data'])){
                if(isset($rank['data'][0])){
                    $data=json_encode($rank['data'][0]);

                    Log::debug("Semrush[0]: ".$data);
                    return ($data);
                }else{
                    $data=json_encode($rank['data']);
                    Log::debug("Semrush: ".$data);
                    return ($data);
                }
            }else{

                return json_encode('{}');
            }

        }else{
            return json_encode('{}');
        }

    }


}