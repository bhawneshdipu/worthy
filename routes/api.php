<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/getAlexaXData', 'GraphController@getAlexaXData');
Route::post('/getAlexaYData', 'GraphController@getAlexaYData');
Route::post('/getSumrushXData', 'GraphController@getSumrushXData');
Route::post('/getSumrushYData', 'GraphController@getSumrushYData');

Route::get('/getAlexaXData', 'GraphController@getAlexaXData');
Route::get('/getAlexaYData', 'GraphController@getAlexaYData');
Route::get('/getSumrushXData', 'GraphController@getSumrushXData');
Route::get('/getSumrushYData', 'GraphController@getSumrushYData');

Route::get('/getXData', 'GraphController@getXData');

Route::get('/getYResponseTime', 'GraphController@getYResponseTime');

Route::get('/getYFacebookCount', 'GraphController@getYFacebookCount');

Route::get('/getYSpeedScore', 'GraphController@getYSpeedScore');

Route::get('/getYMobileUsablityScore', 'GraphController@getYMobileUsabilityScore');
Route::get('/getYPinterest', 'GraphController@getYPinterest');
Route::get('/getYStumbleupon', 'GraphController@getYStumbleupon');

Route::get('/getPageStats', 'GraphController@getPageStats');




/*------------1-------------*/


Route::post('/1/getAlexaXData', 'GraphController@getAlexaXData');
Route::post('/1/getAlexaYData', 'GraphController@getAlexaYData');
Route::post('/1/getSumrushXData', 'GraphController@getSumrushXData');
Route::post('/1/getSumrushYData', 'GraphController@getSumrushYData');

Route::get('/1/getAlexaXData', 'GraphController@getAlexaXData');
Route::get('/1/getAlexaYData', 'GraphController@getAlexaYData');
Route::get('/1/getSumrushXData', 'GraphController@getSumrushXData');
Route::get('/1/getSumrushYData', 'GraphController@getSumrushYData');

Route::get('/1/getXData', 'GraphController@getXData');

Route::get('/1/getYResponseTime', 'GraphController@getYResponseTime');

Route::get('/1/getYFacebookCount', 'GraphController@getYFacebookCount');

Route::get('/1/getYSpeedScore', 'GraphController@getYSpeedScore');

Route::get('/1/getYMobileUsablityScore', 'GraphController@getYMobileUsabilityScore');
Route::get('/1/getYPinterest', 'GraphController@getYPinterest');
Route::get('/1/getYStumbleupon', 'GraphController@getYStumbleupon');

Route::get('/1/getPageStats', 'GraphController@getPageStats');


/*----------2------------*/


Route::post('/2/getAlexaXData', 'GraphController@getAlexaXData');
Route::post('/2/getAlexaYData', 'GraphController@getAlexaYData');
Route::post('/2/getSumrushXData', 'GraphController@getSumrushXData');
Route::post('/2/getSumrushYData', 'GraphController@getSumrushYData');

Route::get('/2/getAlexaXData', 'GraphController@getAlexaXData');
Route::get('/2/getAlexaYData', 'GraphController@getAlexaYData');
Route::get('/2/getSumrushXData', 'GraphController@getSumrushXData');
Route::get('/2/getSumrushYData', 'GraphController@getSumrushYData');

Route::get('/2/getXData', 'GraphController@getXData');

Route::get('/2/getYResponseTime', 'GraphController@getYResponseTime');

Route::get('/2/getYFacebookCount', 'GraphController@getYFacebookCount');

Route::get('/2/getYSpeedScore', 'GraphController@getYSpeedScore');

Route::get('/2/getYMobileUsablityScore', 'GraphController@getYMobileUsabilityScore');
Route::get('/2/getYPinterest', 'GraphController@getYPinterest');
Route::get('/2/getYStumbleupon', 'GraphController@getYStumbleupon');

Route::get('/2/getPageStats', 'GraphController@getPageStats');




Route::get('/1/getweblist',function(){
        $keyword=request()->get('keyword');
        \Illuminate\Support\Facades\Log::debug($keyword);
        $weblist=\App\Website::where('host','like','%'.$keyword.'%')->limit(5)->get();
        $list=array();
        if($weblist!=null && count($weblist)>0){
            foreach ($weblist as $web){
                $list[]=$web->host;
            }
        }
        return response()->json($list);
});
Route::get('/2/getweblist',function(){

    $keyword=request()->get('keyword');

    \Illuminate\Support\Facades\Log::debug($keyword);
    $weblist=\App\Website::where('host','like','%'.$keyword.'%')->limit(5)->get();
    $list=array();
    if($weblist!=null && count($weblist)>0){
        foreach ($weblist as $web){
            $list[]=$web->host;
        }
    }
    return response()->json($list);

});