<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'WebsiteController@index');

Route::get('/checkStore','WebsiteController@store');

Route::post('/validate','WebsiteController@valid');
Route::post('/validateOld','WebsiteController@validOld');

Route::post('/alexa','WebsiteController@alexa');
Route::post('/semrush','WebsiteController@semrush');
Route::post('/facebook','WebsiteController@facebook');
Route::post('/stumbleupon','WebsiteController@stumbleupon');
Route::post('/pinterest','WebsiteController@pinterest');
Route::post('/google','WebsiteController@google');

Route::post('/save','WebsiteController@save');




Route::get('/validate','WebsiteController@valid');

Route::get('/validateOld','WebsiteController@validOld');

Route::get('/alexa','WebsiteController@alexa');
Route::get('/semrush','WebsiteController@semrush');
Route::get('/facebook','WebsiteController@facebook');
Route::get('/stumbleupon','WebsiteController@stumbleupon');
Route::get('/pinterest','WebsiteController@pinterest');
Route::get('/google','WebsiteController@google');

Route::get('/save','WebsiteController@save');



Route::get('/compare',function (){
    return view('compare_page');
});


Route::get('/compare/{web1}/{web2}','CompareController@default');

Route::get('/{website}','GraphController@index');
Route::post('/{website}','GraphController@index');


Route::get('/{web1}/{web2}','CompareController@index');